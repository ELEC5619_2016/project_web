package com.ecle5619.springapp.dao.impl.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.elec5619.springapp.dao.RoleDao;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext*.xml")
public class RoleDaoImplTest {

	@Autowired
	public RoleDao roledao;

	@Test
	public void testFindByUserId() {

		roledao.findByUserid(01);
		roledao.findByUserid(02);
		roledao.findByUserid(03);
		Assert.assertEquals("liudong@hotmail.com", roledao.findByUserid(01));
		Assert.assertEquals("liudong", "doug1111", roledao.findByUserid(01));
		Assert.assertEquals("leon@hotmail.com", roledao.findByUserid(01));
		Assert.assertEquals("leon", "leon1111", roledao.findByUserid(01));
		Assert.assertEquals("hello@hotmail.com", roledao.findByUserid(01));
		Assert.assertEquals("hello", "hello1111", roledao.findByUserid(01));

	}
}
