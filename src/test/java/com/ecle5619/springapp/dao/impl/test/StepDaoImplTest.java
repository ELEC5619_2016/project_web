package com.ecle5619.springapp.dao.impl.test;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.CollectionUtils;

import com.elec5619.springapp.dao.StepDao;
import com.elec5619.springapp.model.Step;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext*.xml")
public class StepDaoImplTest {

	private static long ONE_DAY = 24 * 60 * 60 * 1000l;

	@Autowired
	public StepDao stepDao;

	@Test
	public void testFindByUserId() {
		long currentDate = System.currentTimeMillis() / ONE_DAY * ONE_DAY;
		Date startDate = null;
		Date endDate = new Date(currentDate);
		startDate = new Date(currentDate - 7 * ONE_DAY);
		List<Step> list = stepDao.findByUserId(1, startDate, endDate);
		if(!CollectionUtils.isEmpty(list)){
			for(Step step:list){
				System.out.println(step);
			}
		}

	}

}
