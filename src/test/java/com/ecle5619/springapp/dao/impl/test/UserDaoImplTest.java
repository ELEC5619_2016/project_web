package com.ecle5619.springapp.dao.impl.test;



import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.CollectionUtils;

import com.elec5619.springapp.dao.StepDao;
import com.elec5619.springapp.dao.UserDao;
import com.elec5619.springapp.dao.impl.UserDaoImpl;
import com.elec5619.springapp.model.Step;


import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext*.xml")
public class UserDaoImplTest {

	private static long ONE_DAY = 24 * 60 * 60 * 1000l;

	@Autowired
	public StepDao stepDao;

	@Test
	public void testFindByUserId() {
		long currentDate = System.currentTimeMillis() / ONE_DAY * ONE_DAY;
		Date startDate = null;
		Date endDate = new Date(currentDate);
		startDate = new Date(currentDate - 7 * ONE_DAY);
		List<Step> list = stepDao.findByUserId(1, startDate, endDate);
		if (!CollectionUtils.isEmpty(list)) {
			for (Step step : list) {
				System.out.println(step);
			}
		}

	}

	@Test
	public void testfindByid() {
		UserDao userid = new UserDaoImpl();
		userid.findById(01);
		userid.findById(02);
		userid.findById(03);
		Assert.assertEquals("liudong@hotmail.com", userid.findById(01));
		Assert.assertEquals("liudong", "doug1111", userid.findById(01));
		Assert.assertEquals("leon@hotmail.com", userid.findById(02));
		Assert.assertEquals("leon", "leon1111", userid.findById(02));
		Assert.assertEquals("hello@hotmail.com", userid.findById(01));
		Assert.assertEquals("hello", "hello1111", userid.findById(01));

	}

	@Test
	public void testfindByEmail() {
		UserDao useremail = new UserDaoImpl();
		useremail.findByEmail("liudong@hotmail.com");
		useremail.findByEmail("leon@hotmail.com");
		useremail.findByEmail("hello@hotmail.com");
		Assert.assertEquals(01, useremail.findByEmail("liudoug@hotmail.com"));
		Assert.assertEquals("liudong", "doug1111", useremail.findByEmail("liudoug@hotmail.com"));
		Assert.assertEquals(02, useremail.findByEmail("leon@hotmail.com"));
		Assert.assertEquals("leon", "leon1111", useremail.findByEmail("liudoug@hotmail.com"));
		Assert.assertEquals(03, useremail.findByEmail("hello@hotmail.com"));
		Assert.assertEquals("hello", "hello1111", useremail.findByEmail("liudoug@hotmail.com"));
	}

	@Test
	public void testfindByUsernameAndPassword() {
		UserDao user = new UserDaoImpl();
		user.findByUsernameAndPassword("liudong", "doug1111");
		user.findByUsernameAndPassword("leon", "leon1111");
		user.findByUsernameAndPassword("hello", "hello1111");
		Assert.assertEquals(01, user.findByUsernameAndPassword("liudong", "doug1111"));
		Assert.assertEquals("liudong@hotmail.com", user.findByUsernameAndPassword("liudong", "doug1111"));
		Assert.assertEquals(02, user.findByUsernameAndPassword("leon", "leon1111"));
		Assert.assertEquals("leon@hotmail.com", user.findByUsernameAndPassword("leon", "leon1111"));
		Assert.assertEquals(03, user.findByUsernameAndPassword("liudong", "doug1111"));
		Assert.assertEquals("hello@hotmail.com", user.findByUsernameAndPassword("hello", "hello1111"));
	}



}
