package com.ecle5619.springapp.dao.impl.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.elec5619.springapp.dao.ProfileDao;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext*.xml")
public class ProfileDaoImplTest {

	@Autowired
	public ProfileDao profiledao;

	@Test
	public void testFindByUserId() {

		profiledao.findByUserid(01);
		profiledao.findByUserid(02);
		profiledao.findByUserid(03);
		Assert.assertEquals("liudong@hotmail.com", profiledao.findByUserid(01));
		Assert.assertEquals("liudong", "doug1111", profiledao.findByUserid(01));
		Assert.assertEquals("leon@hotmail.com", profiledao.findByUserid(01));
		Assert.assertEquals("leon", "leon1111", profiledao.findByUserid(01));
		Assert.assertEquals("hello@hotmail.com", profiledao.findByUserid(01));
		Assert.assertEquals("hello", "hello1111", profiledao.findByUserid(01));

	}
}
