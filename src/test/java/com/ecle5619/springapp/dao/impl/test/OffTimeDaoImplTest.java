package com.ecle5619.springapp.dao.impl.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.elec5619.springapp.dao.OffTimeDao;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext*.xml")
public class OffTimeDaoImplTest {

	@Autowired
	public OffTimeDao offtimedao;

	@Test
	public void testfindMaxLengthByUId() {
		offtimedao.findMaxLengthByUId(01);
		offtimedao.findMaxLengthByUId(02);
		offtimedao.findMaxLengthByUId(03);
		offtimedao.findMaxLengthByUId(04);
		Assert.assertEquals(10, offtimedao.findMaxLengthByUId(01));
		Assert.assertEquals(100, offtimedao.findMaxLengthByUId(02));
		Assert.assertEquals(300, offtimedao.findMaxLengthByUId(03));
		Assert.assertEquals(400, offtimedao.findMaxLengthByUId(04));

	}

}
