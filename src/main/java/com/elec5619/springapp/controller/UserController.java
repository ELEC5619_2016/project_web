package com.elec5619.springapp.controller;

import java.io.IOException;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.springapp.dao.UserDao;
import com.elec5619.springapp.dao.ProfileDao;
import com.elec5619.springapp.dao.RoleDao;
import com.elec5619.springapp.model.User;
import com.elec5619.springapp.model.Profile;
import com.elec5619.springapp.model.Role;

import net.sf.json.JSONObject;
import net.sf.json.util.JSONStringer;


@Controller
@RequestMapping("/login")
public class UserController {

	Logger logger = Logger.getLogger(UserController.class);

	@Autowired
	public HttpSession session;
	@Autowired
	public UserDao userDao;
	@Autowired
	public ProfileDao profileDao;
	@Autowired
	public RoleDao roleDao;
	
	@RequestMapping(value = "" , method = { RequestMethod.GET })
	public ModelAndView index() {
		System.out.println("User------------->");
		ModelAndView modelAndView = new ModelAndView("login");
		User user = new User();
		modelAndView.addObject("user", user);
		return modelAndView;
	}
	
	@RequestMapping(value = "/getUser" , method = { RequestMethod.POST })
	@ResponseBody
	public ModelMap mobileLogin(@RequestBody Map<String,String> body) throws IOException {
		System.out.println(body);

		User user = userDao.findByUsernameAndPassword(body.get("username"),
				body.get("password"));
		ModelMap modelMap = new ModelMap();
		if (user == null) {
			System.out.println("loginfail------------------->");
			return modelMap;
		}
		long id = userDao.findByUser(body.get("username"));
		modelMap.addAttribute("id", String.valueOf(id));
		modelMap.addAttribute("username", body.get("username"));
		modelMap.addAttribute("password", body.get("password"));
	
		return modelMap;
	}

	@RequestMapping(value =  "" , method = { RequestMethod.POST })
	public void login(@ModelAttribute("user") User _user,
			HttpServletResponse response) throws IOException {
		System.out.println("get in login------------------->");
		User user = userDao.findByUsernameAndPassword(_user.getUsername(),
				_user.getPassword());
		if (user == null) {
			response.sendRedirect("login");
			session.setAttribute("login_error", "username or password is incorrect");
			System.out.println("loginfail------------------->");
			return;
		}
		System.out.println("loginsuccess--------------->");
		session.setAttribute("sessionUser", user);
		session.setAttribute("username", user.getUsername());
		session.setAttribute("uid", user.getId());
		System.out.println("Get profile-------------------->");
		Profile profile = profileDao.findByUserid((long) session.getAttribute("uid"));
		session.setAttribute("profile", profile);
		response.sendRedirect("/phoneaway");
	}

	@RequestMapping(value =  "logout" , method = { RequestMethod.GET })
	public void logout(HttpServletResponse response) throws IOException {
		session.removeAttribute("sessionUser");
		session.removeAttribute("username");
		session.removeAttribute("uid");
		session.removeAttribute("profile");
		System.out.println("logout------------------>");
		response.sendRedirect("/phoneaway");
	}

	@RequestMapping(value = "signup", method = { RequestMethod.POST })
	public String create(@ModelAttribute("user") User user,
			HttpServletResponse response) throws IOException {
		
		long exist = userDao.findByUser(user.getUsername());
		if (exist != -1) {
			session.setAttribute("signup_fail", "Username has been taken!");
			System.out.println("Signup fail same username-------------------->");
			return "redirect:/login";
		}
		
		User nuser = userDao.findByEmail(user.getEmail());
		if (nuser != null) {
			session.setAttribute("signup_fail", "Email address has been taken!");
			System.out.println("Signup fail same email address-------------------->");
			return "redirect:/login";
		}
		
		userDao.save(user);
		System.out.println("Signup success-------------------->");
		session.setAttribute("sessionUser", user);
		session.setAttribute("username", user.getUsername());
		session.setAttribute("uid", user.getId());
		
		Role role = new Role();
		role.setUserid(user.getId());
		role.setRolename("user");
		roleDao.save(role);
		System.out.println("Role create success-------------------->");
		
		Profile profile = new Profile();
		profile.setUserid(user.getId());
		profileDao.save(profile);
		System.out.println("Create profile-------------------->");
		session.setAttribute("profile", profile);
		return "redirect:/";
	}

	@RequestMapping(value =  "updateuser" , method = { RequestMethod.POST })
	public String update(@ModelAttribute("user") User user) {
		System.out.println("Get in user update------------------->");
		User nuser = user;
		
		nuser.setId((long) session.getAttribute("uid"));
		nuser.setUsername((String) session.getAttribute("username"));
		
		User getCreatedate = (User) session.getAttribute("sessionUser");
		nuser.setCreate_date(getCreatedate.getCreate_date());

		userDao.update(nuser);
		System.out.println("User update success--------------->");
		
		session.setAttribute("user_update", "Account information update successfully!");
		
		session.setAttribute("sessionUser", nuser);
		return "redirect:/profile";
	}

}
