package com.elec5619.springapp.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.springapp.dao.AppDao;
import com.elec5619.springapp.dao.OffTimeDao;
import com.elec5619.springapp.model.AppUsage;
import com.elec5619.springapp.model.OffTime;
import com.elec5619.springapp.model.User;


@Controller
@RequestMapping("/")
public class HomeController {
	Logger logger = Logger.getLogger(HomeController.class);
	
	@Autowired
	public OffTimeDao offTimeDao;
	@Autowired
	public AppDao appDao;
	@Autowired
	public HttpSession session;
	
	@RequestMapping(value = "" , method = { RequestMethod.GET })
	public ModelAndView index() {
		System.out.println("Enter------------->");
		ModelAndView modelAndView = new ModelAndView("index");
		return modelAndView;
	}
	
	@RequestMapping(value = "leaderboard" , method = { RequestMethod.GET })
	public ModelAndView leaderboard(HttpServletRequest request) {
		System.out.println(session.getAttribute("offtimes"));
		ModelAndView modelAndView = new ModelAndView("leaderboard");
		if(session.getAttribute("uid") == null){
			System.out.println("no uid------------------->");
			session.removeAttribute("session Time");
			return modelAndView;
		}
			
		OffTime offtime = offTimeDao.findMaxLengthByUId((long)session.getAttribute("uid"));
		AppUsage appUsage = appDao.findMostUsageByUId((long)session.getAttribute("uid"));
		String name = offTimeDao.findUserByDone().getUser().getUsername();
		if (offtime == null) {
			session.setAttribute("h_length", 0);
			System.out.println("query failed------------------->");
			return modelAndView;
		}
		if (appUsage == null) {
			session.setAttribute("m_usage", 0);
			System.out.println("query failed------------------->");
			return modelAndView;
		}
		session.setAttribute("d_username", "user1");

		session.setAttribute("h_length", offtime.getLength());
		session.setAttribute("m_usage", appUsage.getAname());
		if(session.getAttribute("offtimes") != null){
			List<OffTime> offtimes = new ArrayList<>();
			offtimes =(List<OffTime>) session.getAttribute("offtimes");
			String length = "";
			String period ="";
			for(int i =0; i< offtimes.size(); i++){
				length += offtimes.get(i).getLength()+",";
				period += "'"+offtimes.get(i).getDate()+"',";
				if(offtimes.size() - 1 == i){
					length += offtimes.get(i).getLength();
					period += "'"+offtimes.get(i).getDate()+"'";
				}
			}
			System.out.println(length);
			System.out.println(period);
			request.setAttribute("length", length);
			request.setAttribute("period", period);
		}else{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			
			List<OffTime> list = offTimeDao.findByDate(sdf.format(date), sdf.format(date), (long)session.getAttribute("uid"));
			String length = "";
			String period ="";
			if(list != null){
				for(int i =0; i< list.size(); i++){
					length += list.get(i).getLength()+",";
					period += "'"+list.get(i).getDate()+"',";
					if(list.size() - 1 == i){
						length += list.get(i).getLength();
						period += "'"+list.get(i).getDate()+"'";
					}
				}
			}
		
			System.out.println(length);
			System.out.println(period);
			request.setAttribute("length", length);
			request.setAttribute("period", period);
		}
		
		if(session.getAttribute("apps") != null){
			List<AppUsage> apps = new ArrayList<>();
			apps =(List<AppUsage>) session.getAttribute("apps");
			String slice = "";
			String pname ="";
			for(int i =0; i< apps.size(); i++){
				slice += apps.get(i).getCount()+",";
				pname += "'"+apps.get(i).getAname()+"',";
				if(apps.size() - 1 == i){
					slice += apps.get(i).getCount();
					pname += "'"+apps.get(i).getAname()+"'";
				}
			}
			System.out.println(slice);
			System.out.println(pname);
			request.setAttribute("slice", slice);
			request.setAttribute("pname", pname);
		}else{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			
			List<AppUsage> list = appDao.findByDate(sdf.format(date), sdf.format(date), (long)session.getAttribute("uid"));
			String slice = "";
			String pname ="";
			if(list != null){
				for(int i =0; i< list.size(); i++){
					slice += list.get(i).getCount()+",";
					pname += "'"+list.get(i).getAname()+"',";
					if(list.size() - 1 == i){
						slice += list.get(i).getCount();
						pname += "'"+list.get(i).getAname()+"'";
					}
				}
			}
			
			System.out.println(slice);
			System.out.println(pname);
			request.setAttribute("slice", slice);
			request.setAttribute("pname", pname);
		}
	
		return modelAndView;
	}
	
	
	@RequestMapping(value = "about" , method = { RequestMethod.GET })
	public ModelAndView about() {
		ModelAndView modelAndView = new ModelAndView("about");
		return modelAndView;
	}
	
	@RequestMapping(value = "policy" , method = { RequestMethod.GET })
	public ModelAndView policy() {
		ModelAndView modelAndView = new ModelAndView("policy");
		return modelAndView;
	}
	
	@RequestMapping(value = "termuse" , method = { RequestMethod.GET })
	public ModelAndView termuse() {
		ModelAndView modelAndView = new ModelAndView("termuse");
		return modelAndView;
	}

}
