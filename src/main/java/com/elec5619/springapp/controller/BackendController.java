package com.elec5619.springapp.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.springapp.dao.OffTimeDao;
import com.elec5619.springapp.dao.ProfileDao;
import com.elec5619.springapp.dao.RoleDao;
import com.elec5619.springapp.dao.UserDao;
import com.elec5619.springapp.model.OffTime;
import com.elec5619.springapp.model.Profile;
import com.elec5619.springapp.model.User;
import com.elec5619.springapp.model.Role;


@Controller
@RequestMapping("/backend")
public class BackendController {
	Logger logger = Logger.getLogger(BackendController.class);
	
	@Autowired
	public HttpSession session;
	@Autowired
	public UserDao userDao;
	@Autowired
	public ProfileDao profileDao;
	@Autowired
	public OffTimeDao offTimeDao;
	@Autowired
	public RoleDao roleDao;

	
	@RequestMapping(value = "" , method = { RequestMethod.GET })
	public ModelAndView index() {
		System.out.println("Enter Backend------------->");
		ModelAndView modelAndView = new ModelAndView("backend/login");
		User user = new User();
		modelAndView.addObject("user", user);
		
		return modelAndView;
	}
	
	@RequestMapping(value = "main" , method = { RequestMethod.GET })
	public ModelAndView Loadmain() {
		System.out.println("Redirect main------------->");
		ModelAndView modelAndView = new ModelAndView("backend/main");
		
		User user = (User) session.getAttribute("sessionUser");
		modelAndView.addObject("user", user);

		List<User> list = userDao.findallusers();
		modelAndView.addObject("userlist", list);

		return modelAndView;
	}
	
	@RequestMapping(value = "data" , method = { RequestMethod.GET })
	public ModelAndView LoadData() {
		System.out.println("Redirect data------------->");
		ModelAndView modelAndView = new ModelAndView("backend/data");
		
		User user = (User) session.getAttribute("sessionUser");
		modelAndView.addObject("user", user);
		
		List<OffTime> offtimes = offTimeDao.findAll();
	
		modelAndView.addObject("offtimeList", offtimes);

		return modelAndView;
	}
	
	@RequestMapping(value = "data/modify" , method = { RequestMethod.POST })
	public ModelAndView modifyData(String id, String done, String length) {
		System.out.println("delete" +id);
		offTimeDao.delete(Integer.valueOf(id));
		ModelAndView modelAndView = new ModelAndView("redirect:/backend/data");
		
		User user = (User) session.getAttribute("sessionUser");
		modelAndView.addObject("user", user);

		return modelAndView;
	}
	
	
	@RequestMapping(value =  "" , method = { RequestMethod.POST })
	public ModelAndView login(@ModelAttribute("user") User _user) {
		System.out.println("get in admin login------------------->");
		User user = userDao.findByUsernameAndPassword(_user.getUsername(),
				_user.getPassword());
		if (user == null || user.getUsername().compareTo("admin") != 0) {
			session.setAttribute("login_error", "username or password is incorrect");
			System.out.println("admin loginfail------------------->");
			ModelAndView modelAndView = new ModelAndView("backend/login");
			return modelAndView;
		}
		System.out.println("admin loginsuccess--------------->");
		session.setAttribute("sessionUser", user);
		ModelAndView modelAndView = new ModelAndView("backend/main");

		List<User> list = userDao.findallusers();
		modelAndView.addObject("userlist", list);
		

		return modelAndView;
	}

	@RequestMapping(value =  "search" , method = { RequestMethod.POST })
	public ModelAndView search(@ModelAttribute("user") User user) {
		System.out.println("get in admin search------------------->");
		
		List<User> list = userDao.findByUsername(user.getUsername());

		if (list == null) {
			session.setAttribute("search_fail", "User not found!");
			System.out.println("admin search fail------------------->");
			
			ModelAndView modelAndView = new ModelAndView("backend/main");
			
			User nuser = (User) session.getAttribute("sessionUser");
			modelAndView.addObject("user", nuser);
			
			list = userDao.findallusers();
			modelAndView.addObject("userlist", list);

			return modelAndView;
		}
		System.out.println("admin search success--------------->");
		
		ModelAndView modelAndView = new ModelAndView("backend/main");
		
		User nuser = (User) session.getAttribute("sessionUser");
		modelAndView.addObject("user", nuser);
		modelAndView.addObject("userlist", list);

		return modelAndView;
	}

	@RequestMapping(value = "logout" , method = { RequestMethod.GET })
	public void logout(HttpServletResponse response) throws IOException {
		session.removeAttribute("sessionUser");
		System.out.println("logout------------------>");
		response.sendRedirect("/phoneaway/backend");
	}
	
	@RequestMapping(value =  "modify" , method = { RequestMethod.POST })
	public String backendUpdate(@ModelAttribute("user") User user, @RequestParam String action) {
		if (action.equals("Update")) {
			System.out.println("Get in backend user update------------------->");
			User nuser = user;
			long userId = userDao.findByUser((String) user.getUsername());

			nuser.setId(userId);
			
			User getCreatedate = (User) userDao.findById(userId);
			nuser.setCreate_date(getCreatedate.getCreate_date());

			userDao.update(nuser);
			System.out.println("Backend user update success--------------->");

			session.setAttribute("user_update", "User account information update successfully!");
			
			return "redirect:/backend/main";
		} else if (action.equals("Delete")) {
			System.out.println("Get in backend user delete------------------->");
			long userId = userDao.findByUser(user.getUsername());
			
			if (userId == -1) {
				User nuser = userDao.findByEmail(user.getEmail());
				userId = nuser.getId();
			}
			
			if (profileDao.findByUserid(userId) != null) {
				Profile profile = profileDao.findByUserid(userId);
				profileDao.delete(profile);
			}
			
			if (roleDao.findByUserid(userId) != null) {
				Role role = roleDao.findByUserid(userId);
				roleDao.delete(role);
			}

			userDao.delete(userId);
			System.out.println("Backend user delete success--------------->");

			session.setAttribute("user_delete", "User account information delete successfully!");
			
			return "redirect:/backend/main";
		} else {
			System.out.println("Get in backend add user------------------->");
			
			long exist = userDao.findByUser(user.getUsername());
			if (exist != -1) {
				session.setAttribute("user_add", "Add user fail! Username has been taken!");
				System.out.println("Backend add user duplicated same username-------------------->");
				return "redirect:/backend/main";
			}
			
			User nuser = userDao.findByEmail(user.getEmail());
			if (nuser != null) {
				session.setAttribute("user_add", "Add user fail! Email address has been taken!");
				System.out.println("Backend add user fail duplicated email address-------------------->");
				return "redirect:/backend/main";
			}
			
			userDao.save(user);
			System.out.println("Backend add user success-------------------->");
			
			Profile profile = new Profile();
			profile.setUserid(user.getId());
			profileDao.save(profile);
			System.out.println("Create profile-------------------->");

			session.setAttribute("user_add", "Add user account successfully!");
			return "redirect:/backend/main";
		}

	}
}
