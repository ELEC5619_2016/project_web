package com.elec5619.springapp.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.springapp.dao.StepDao;
import com.elec5619.springapp.dao.UserDao;
import com.elec5619.springapp.model.Step;

@Controller
@RequestMapping("/step")
public class StatisticController {
	@Autowired
	public HttpSession session;
	@Autowired
	public StepDao stepDao;
	@Autowired
	public UserDao userDao;
	
	private static long ONE_DAY = 24*60*60*1000l;
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public Step addStep(@RequestBody Map<String,String> body) {
		
		System.out.println("step body=" + body);
		Step step = new Step();
		step.setSteps(body.get("steps"));
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    try {
	    	System.out.println("step body=" + body.get("date"));
	    	Date date = (Date) formatter.parse(body.get("date"));
	    	
	    	step.setDate(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		step.setUser(userDao.findById(Integer.valueOf(body.get("uid"))));
		if(null != step.getUser()){
			stepDao.save(step);
		}
	    return step;
	}
	
	@RequestMapping(value = "statistic", method = { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView getStatisticData(String period) {
		ModelAndView mv = new ModelAndView();
		Long userId = (Long)session.getAttribute("uid");;	
		if(null == userId){
			mv.setViewName("/phoneaway/login");
			return mv;
		}
		
		mv.setViewName("statistic");
		
		long currentDate = System.currentTimeMillis()/ONE_DAY*ONE_DAY;
		Date startDate = null;
		Date endDate = new Date(currentDate);
		if("week".equals(period)){
			startDate = new Date(currentDate-7*ONE_DAY);
		}else if("month".equals(period)){
			startDate = new Date(currentDate-30*ONE_DAY);
		}else{
			startDate = new Date(currentDate-7*ONE_DAY);
		}
		System.out.println(period);
		System.out.println(startDate);
		System.out.println(endDate);

		List<Step> list = stepDao.findByUserId(userId, startDate, endDate);
		mv.addObject("statistic", list);
		SimpleDateFormat simpleDateFormat =new SimpleDateFormat("yyyy-MM-dd");

		mv.addObject("sdate",simpleDateFormat.format(startDate));
		mv.addObject("edate",simpleDateFormat.format(endDate));
		
		return mv;
	}
	
	
	@RequestMapping(value = "statistic/query", method = { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView getStatisticDataBByDateArea(String sdate,String edate) {
		ModelAndView mv = new ModelAndView();
		Long userId = (Long)session.getAttribute("uid");;	
		if(null == userId){
			mv.setViewName("/phoneaway/login");
			return mv;
		}
		
		
		mv.setViewName("statistic");
		System.out.println(sdate+"-------"+edate);
		
		Date startDate = null;
		Date endDate = null;
		
		SimpleDateFormat simpleDateFormat =new SimpleDateFormat("yyyy-MM-dd");
		
		try{
			startDate = simpleDateFormat.parse(sdate);
			endDate = simpleDateFormat.parse(edate);
		}catch (Exception e) {
			return mv;
		}
		
		List<Step> list = stepDao.findByUserId(userId, startDate, endDate);
		mv.addObject("statistic", list);
		mv.addObject("sdate",sdate);
		mv.addObject("edate",edate);
		
		
		return mv;
	}
}
