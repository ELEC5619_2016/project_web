package com.elec5619.springapp.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.springapp.dao.OffTimeDao;
import com.elec5619.springapp.dao.StepDao;
import com.elec5619.springapp.dao.UserDao;
import com.elec5619.springapp.model.OffTime;
import com.elec5619.springapp.model.Step;


@Controller
@RequestMapping("/achievement")
public class AchievementController {
	Logger logger = Logger.getLogger(AchievementController.class);

	@Autowired
	public HttpSession session;
	@Autowired
	public OffTimeDao offTimeDao;
	@Autowired
	public UserDao userDao;
	@Autowired
	public StepDao stepDao;
	
	@RequestMapping(value = "" , method = { RequestMethod.GET })
	public ModelAndView index() {
		System.out.println("Enter Backend------------->");
		ModelAndView modelAndView = new ModelAndView("achievement");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		List<OffTime> lists = offTimeDao.findOfftimeByLength(formatter.format(cal.getTime()));
		List<Step> steps = stepDao.findStepBySteps(formatter.format(cal.getTime()));
	
		modelAndView.addObject("achievementList", lists);
		modelAndView.addObject("stepList", steps);

		return modelAndView;
	}
	
	@RequestMapping(value = "" , method = { RequestMethod.POST })
	public ModelAndView index(String date) {
		System.out.println(date);
		ModelAndView modelAndView = new ModelAndView("achievement");
		List<OffTime> lists = offTimeDao.findOfftimeByLength(date);
		List<Step> steps = stepDao.findStepBySteps(date);
	
		modelAndView.addObject("achievementList", lists);
		modelAndView.addObject("stepList", steps);

		return modelAndView;
	}
}
