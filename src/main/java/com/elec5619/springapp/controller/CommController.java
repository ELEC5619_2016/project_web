package com.elec5619.springapp.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.springapp.dao.CommentDao;
import com.elec5619.springapp.dao.TopicDao;
import com.elec5619.springapp.dao.UserDao;
import com.elec5619.springapp.model.Comment;
import com.elec5619.springapp.model.Topic;
import com.elec5619.springapp.model.User;

@Controller
@RequestMapping("/community")
public class CommController 
{
	Logger logger = Logger.getLogger(CommController.class);

	@Autowired
	public HttpSession session;
	@Autowired
	public TopicDao topicDao;
	@Autowired
	public CommentDao commentDao;
    
	private int cid;
	private int cpage;
	

	@RequestMapping(value = "" , method = { RequestMethod.GET })
	public ModelAndView index()
	{
		System.out.println("comm------>");
		ModelAndView modelAndView = new ModelAndView("Community");
		List<Topic> list=topicDao.queryAll("select u from topic u order by postdate desc");
		modelAndView.addObject("topiclist", list);
		return modelAndView;
	}
	
	@RequestMapping(value = "/get" , method = { RequestMethod.GET })
	public ModelAndView ViewDetails(int id)
	{
		Topic t = topicDao.queryOne("Topic", id);
		ModelAndView modelAndView = new ModelAndView("TopicDetails");
		modelAndView.addObject("topic", t);
		List<Comment> commlist=commentDao.queryAll("select u from comment u where topicid="+t.getId()+" order by commentdate");
		modelAndView.addObject("commlist", commlist);
		return modelAndView;
	}
	
	@RequestMapping(value = "/create" , method = { RequestMethod.GET })
	public ModelAndView NewTopic() 
	{
		ModelAndView modelAndView = new ModelAndView("NewTopic");
		Topic topic = new Topic();
		modelAndView.addObject("topic", topic);
		return modelAndView;
	}
	
	@RequestMapping(value = "/create" , method = { RequestMethod.POST })
	public void AddTopic(@ModelAttribute("topic") Topic topic,
			HttpServletResponse response) throws IOException 
	{
		/*Date now = new Date(); 
		topic.setPostdate(now);*/
		User user=(User)session.getAttribute("sessionUser");
		topic.setUser(user);
		topicDao.add(topic);
		response.sendRedirect("/phoneaway/community");
	}
	
	@RequestMapping(value = "/comment" , method = { RequestMethod.POST })
	public void AddComment(@ModelAttribute("comment") Comment comment,
			HttpServletResponse response) throws IOException 
	{
		/*Date now = new Date(); 
		topic.setPostdate(now);*/
		User user=(User)session.getAttribute("sessionUser");
		comment.setUser(user);
		commentDao.add(comment);
		response.sendRedirect("/phoneaway/TopicDetails");
	}
//
//	@RequestMapping(value = "/delete" , method = { RequestMethod.POST })
//	public void DeleteComment(int cid,
//			HttpServletResponse response) throws IOException 
//	{
//		/*Date now = new Date(); 
//		topic.setPostdate(now);*/
//		Comment cdel = commentDao.queryOne("Comments", cid);
//		commentDao.del(cdel);
//		response.sendRedirect("/phoneaway/TopicDetails");
//	}
	
}
