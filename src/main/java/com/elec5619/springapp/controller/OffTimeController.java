package com.elec5619.springapp.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.springapp.dao.AppDao;
import com.elec5619.springapp.dao.OffTimeDao;
import com.elec5619.springapp.dao.UserDao;
import com.elec5619.springapp.model.AppUsage;
import com.elec5619.springapp.model.OffTime;
import com.elec5619.springapp.model.User;

@Controller
@RequestMapping("/offtime")
public class OffTimeController {
	
	Logger logger = Logger.getLogger(OffTimeController.class);

	@Autowired
	public HttpSession session;
	@Autowired
	public OffTimeDao offTimeDao;
	@Autowired
	public UserDao userDao;
	@Autowired
	public AppDao appDao;
	
	@RequestMapping(value =  "/query" , method = { RequestMethod.POST })
	public ModelAndView query(String sdate, String edate, HttpServletRequest request)  {
		System.out.println("sdate is:"+sdate);
        System.out.println("edate is:"+edate);
		List<OffTime> list = offTimeDao.findByDate(sdate, edate, (long)session.getAttribute("uid"));
		List<AppUsage> apps = appDao.findByDate(sdate, edate, (long)session.getAttribute("uid"));
		ModelAndView modelAndView = new ModelAndView("redirect:/leaderboard?a=");
		modelAndView.addObject("offtimes", list);
		session.setAttribute("offtimes", list);
		modelAndView.addObject("apps", apps);
		session.setAttribute("apps", apps);
		return modelAndView;
	}
	
	@RequestMapping(value = "/queryTimeList" , method = { RequestMethod.POST })
	@ResponseBody
	public ModelMap mobileLogin(@RequestBody Map<String,String> body) throws IOException {
		System.out.println("sdate is:"+body.get("sdate"));
        System.out.println("edate is:"+body.get("edate"));

		List<OffTime> list = offTimeDao.findByDate(body.get("sdate"), body.get("edate"), Integer.valueOf(body.get("uid")));
	    ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("list", list);
	  
		return modelMap;
	}
	
//	@RequestMapping(value = "", method = RequestMethod.POST)
//	@ResponseBody
//	public String delete() {
//		System.out.println("---->");
//		
//		OffTime offtime = new OffTime();
//		offtime.setLength("12s");
//	
//		offTimeDao.save(offtime);
//	    return "123";
//	}
	
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public OffTime addTime(@RequestBody Map<String,String> body) {
		
		System.out.println("time body=" + body);
		OffTime offtime = new OffTime();
		offtime.setLength(body.get("length"));
		offtime.setDone(body.get("done"));
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    try {
	    	Date date = (Date) formatter.parse(body.get("date"));
	    	offtime.setDate(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		offtime.setUser(userDao.findById(Integer.valueOf(body.get("uid"))));
		offTimeDao.save(offtime);
	    return offtime;
	}
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/getTime", method = RequestMethod.POST)
	@ResponseBody
	public ModelMap getTime(@RequestBody Map<String,String> body) {
		
		System.out.println("time body=" + body);
		OffTime offtime = new OffTime();
		offtime.setLength(body.get("length"));
		offtime.setDone(body.get("done"));
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    try {
	    	Date date = (Date) formatter.parse(body.get("odate"));
	    	offtime.setDate(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		offtime.setUser(userDao.findById(Integer.valueOf(body.get("uid"))));
		
		
		ModelMap modelMap = new ModelMap();
//		if (user == null) {
//			System.out.println("loginfail------------------->");
//			return modelMap;
//		}
		long id = userDao.findByUser(body.get("username"));
		modelMap.addAttribute("id", String.valueOf(id));
		modelMap.addAttribute("username", body.get("username"));
		modelMap.addAttribute("password", body.get("password"));
	
		return modelMap;
	}

}
