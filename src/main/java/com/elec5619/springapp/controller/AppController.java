package com.elec5619.springapp.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.elec5619.springapp.dao.AppDao;
import com.elec5619.springapp.dao.UserDao;
import com.elec5619.springapp.model.AppUsage;
import com.elec5619.springapp.model.OffTime;

@Controller
@RequestMapping("/app")
public class AppController {
	Logger logger = Logger.getLogger(AppController.class);

	@Autowired
	public HttpSession session;
	@Autowired
	public AppDao appDao;
	@Autowired
	public UserDao userDao;
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public AppUsage addUsage(@RequestBody Map<String,String> body) {
		
		System.out.println("app body=" + body);
		AppUsage app = new AppUsage();
		
		app.setAname(body.get("aname"));
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    try {
	    	Date date = (Date) formatter.parse(body.get("adate"));
	    	app.setDate(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    AppUsage appUsage = appDao.countApp(body.get("aname"));
		if(appUsage == null || appUsage.getCount().equals("0")){
			app.setCount("1");
			app.setUser(userDao.findById(Integer.valueOf(body.get("uid"))));
			appDao.save(app);
		}else{
			app.setCount(String.valueOf(Integer.valueOf(appUsage.getCount())+1));
			app.setUser(userDao.findById(Integer.valueOf(body.get("uid"))));
			app.setId(appUsage.getId());
			appDao.update(app);
		}
		
	    return app;
	}
	
	@RequestMapping(value = "/queryAppList" , method = { RequestMethod.POST })
	@ResponseBody
	public ModelMap queryAppList(@RequestBody Map<String,String> body) throws IOException {
		System.out.println("APPsdate is:"+body.get("sdate"));
        System.out.println("APPedate is:"+body.get("edate"));

		List<AppUsage> list = appDao.findByDate(body.get("sdate"), body.get("edate"), Integer.valueOf(body.get("uid")));
	    ModelMap modelMap = new ModelMap();
	    System.out.println("app body=" + body);
		modelMap.addAttribute("list", list);
	  
		return modelMap;
	}
	
}
