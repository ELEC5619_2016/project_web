package com.elec5619.springapp.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.node.TextNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.springapp.dao.ProfileDao;
import com.elec5619.springapp.dao.UserDao;
import com.elec5619.springapp.model.Profile;
import com.elec5619.springapp.model.User;

import net.sf.json.JSONObject;
import net.sf.json.util.JSONStringer;


@Controller
@RequestMapping("/profile")
public class ProfileController {

	Logger logger = Logger.getLogger(ProfileController.class);

	@Autowired
	public HttpSession session;
	@Autowired
	public ProfileDao profileDao;
	@Autowired
	public UserDao userDao;

	@RequestMapping(value = "" , method = { RequestMethod.GET })
	public ModelAndView index(HttpServletRequest request) {
		System.out.println("profile-------------------->");
		Profile profile = (Profile) session.getAttribute("profile");
		User user = (User) session.getAttribute("sessionUser");
		ModelAndView modelAndView = new ModelAndView("profile");
		request.setAttribute("user", user);
		modelAndView.addObject("profile", profile);
		modelAndView.addObject("sessionUser", user);
		return modelAndView;
	}

	@RequestMapping(value =  "updateprofile" , method = { RequestMethod.POST })
	public String update(@ModelAttribute("profile") Profile profile) {
		System.out.println("Get in profile update------------------->");
		Profile nprofile = profile;
		Profile getId = (Profile) session.getAttribute("profile");
		long oldID = getId.getId();
		nprofile.setId(oldID);
		nprofile.setUserid((long) session.getAttribute("uid"));
		profileDao.update(nprofile);
		System.out.println("Profile update success--------------->");
		
		session.setAttribute("profile_update", "Profile information update successfully!");
		
		session.setAttribute("profile", nprofile);
		return "redirect:/profile";
	}

}
