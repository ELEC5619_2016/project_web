package com.elec5619.springapp.dao;

import com.elec5619.springapp.model.Role;

public interface RoleDao {

	void save(Role role);

	void delete(Role role);

	void update(Role role);

	Role findByUserid(long userid);
}