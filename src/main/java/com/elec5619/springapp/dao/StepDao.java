package com.elec5619.springapp.dao;

import java.util.Date;
import java.util.List;

import com.elec5619.springapp.model.Step;

public interface StepDao {

	void save(Step step);

	void delete(String id);

	void delete(Step statistic);

	void update(Step statistic);

	Step findById(String id);
	
	List<Step> findByUserId(long userId);
	
	List<Step> findByUserId(long userId,Date startDate,Date endDate);
	
	List<Step> findStepBySteps(String date);
}