package com.elec5619.springapp.dao;

import com.elec5619.springapp.model.Profile;

public interface ProfileDao {

	void save(Profile profile);

	void delete(Profile profile);

	void update(Profile profile);

	Profile findByUserid(long userid);
}