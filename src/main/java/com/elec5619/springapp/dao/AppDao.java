package com.elec5619.springapp.dao;

import java.util.List;

import com.elec5619.springapp.model.AppUsage;
import com.elec5619.springapp.model.OffTime;


public interface AppDao {
	void save(AppUsage usage);

	void delete(AppUsage usage);

	void update(AppUsage usage);
	
	List<AppUsage> findByDate(String sdate, String edate, long uid);
	
	AppUsage findMostUsageByUId(long uid);
	
	AppUsage countApp(String name);
}
