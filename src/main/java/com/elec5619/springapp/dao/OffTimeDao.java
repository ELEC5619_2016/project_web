package com.elec5619.springapp.dao;


import java.util.List;

import com.elec5619.springapp.model.OffTime;


public interface OffTimeDao{
	
	void save(OffTime offtime);

	void delete(long id);

	void delete(OffTime offtime);

	void update(OffTime offtime);

	List<OffTime> findByDate(String sdate, String edate, long uid);
	
	OffTime findMaxLengthByUId(long uid);
	
	List<OffTime> findOfftimeByLength(String date);
	
	OffTime findAppByUId(long uid);
	
	List<OffTime> findAll();

	OffTime findById(long id);

	OffTime findUserByDone();
}
