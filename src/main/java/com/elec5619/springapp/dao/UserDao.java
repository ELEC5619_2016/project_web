package com.elec5619.springapp.dao;

import java.util.List;

import com.elec5619.springapp.model.OffTime;
import com.elec5619.springapp.model.User;

public interface UserDao {

	void save(User user);

	void delete(long id);

	void delete(User user);

	void update(User user);

	User findById(long i);
	
	User findByEmail(String email);
	
	long findByUser(String username);

	User findByUsernameAndPassword(String username, String password);
	
	List<User> findallusers();
	
	List<User> findByUsername(String username);
	
}