package com.elec5619.springapp.dao;
import java.util.List;

import com.elec5619.springapp.model.AppUsage;
import com.elec5619.springapp.model.Topic;
public interface TopicDao{
	void add(Topic topic);
	public List<Topic> queryAll(String hql);
	public Topic queryOne(String hql,Integer id);
}
