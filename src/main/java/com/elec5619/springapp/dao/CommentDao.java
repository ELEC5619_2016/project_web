package com.elec5619.springapp.dao;
import java.util.List;

import com.elec5619.springapp.model.Comment;
import com.elec5619.springapp.model.Topic;
import com.elec5619.springapp.model.User;

public interface CommentDao{
	void add(Comment comment);
	public List<Comment> queryAll(String hql);
	public Comment queryOne(String hql,Integer id);
}

