package com.elec5619.springapp.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.springapp.dao.ProfileDao;
import com.elec5619.springapp.model.Profile;

@Transactional
@Repository
public class ProfileDaoImpl implements ProfileDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Resource
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void save(Profile profile) {
		getSession().save(profile);
	}

	@Override
	public void delete(Profile profile) {
		getSession().delete(profile);
	}

	@Override
	public void update(Profile profile) {
		getSession().update(profile);
	}

	@Override
	public Profile findByUserid(long userid) {
		String hql = "from Profile where userid=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, userid);
		List<Profile> list = query.list();
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

}
