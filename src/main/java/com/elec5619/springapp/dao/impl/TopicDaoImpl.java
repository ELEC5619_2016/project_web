package com.elec5619.springapp.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.springapp.dao.TopicDao;
import com.elec5619.springapp.model.Comment;
import com.elec5619.springapp.model.Topic;

@Transactional
@Repository
public class TopicDaoImpl implements TopicDao
{
	@Autowired
	private SessionFactory sessionFactory;

	@Resource
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	

	@Override
	public void add(Topic topic) {
		// TODO Auto-generated method stub
		getSession().save(topic);
	}

	@Override
	public List<Topic> queryAll(String hql) {
		Session session = null;
		Transaction ts = null;
		List<Topic>list = null;
		try{
			session = getSession();
			ts = session.beginTransaction();
			Query query = session.createQuery(hql);
			list = query.list();
			ts.commit();
			
		}catch(Exception d){
			ts.rollback();
		}finally{
			session.close();
		}
		return list;
	}

	@Override
	public Topic queryOne(String hql, Integer id) {
		Session session = null;

		Transaction ts = null;
		List<Topic> list = null;
		try {
			session = getSession();
			ts = session.beginTransaction();
			Query query = session.createQuery("select u from " + hql
					+ " u where u.id=" + id);
			list = query.list();
			ts.commit();
		} catch (Exception d) {
			ts.rollback();
		} finally {
			session.close();
		}
		return list.get(0);
	}
}
