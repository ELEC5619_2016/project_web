package com.elec5619.springapp.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.elec5619.springapp.dao.StepDao;
import com.elec5619.springapp.model.OffTime;
import com.elec5619.springapp.model.Step;

@Transactional
@Repository
public class StepDaoImpl implements StepDao{

	@Autowired
	private SessionFactory sessionFactory;

	@Resource
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void save(Step step) {
		getSession().save(step);
	}

	@Override
	public void delete(String id) {
		Step step = findById(id);
		if(null != step){
			getSession().delete(step);
		}
	}

	@Override
	public void delete(Step step) {
		getSession().delete(step);
	}

	@Override
	public void update(Step step) {
		getSession().update(step);
	}

	@Override
	public Step findById(String id) {
		String hql = "from Statistic where id=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, id);
		
		@SuppressWarnings("unchecked")
		List<Step> list = query.list();
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public List<Step> findByUserId(long userId) {
		String hql = "from Step where user_id=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, userId);
		
		@SuppressWarnings("unchecked")
		List<Step> list = query.list();
		
		return list;
	}

	@Override
	public List<Step> findByUserId(long userId, Date startDate, Date endDate) {
		String hql = "from Step where user_id=? and sdate >= ? and sdate <= ?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, userId);
		query.setParameter(1, startDate);
		query.setParameter(2, endDate);
		
		@SuppressWarnings("unchecked")
		List<Step> list = query.list();
		
		return list;
	}
	
	public List<Step> findStepBySteps(String date) {
		String hql = "from Step where date>=? and date<=? group by user.id";
		Query query = getSession().createQuery(hql);
		if(date.indexOf("/")!= -1){
			date = date.replace("/", "-");
		}

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	    try {
	    	Date s_date = (Date) formatter.parse(date);
			Date e_date = (Date) formatter.parse(date);
			Calendar cal = Calendar.getInstance();
			cal.setTime(e_date);
			cal.add(Calendar.DATE, 1);
		
			System.out.println("sdate is:"+s_date);
	        System.out.println("edate is:"+e_date);
			query.setParameter(0, s_date);
			query.setParameter(1, cal.getTime());

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Step> list = query.list();
		System.out.println(list);

		if (list == null || list.size() == 0) {
			System.out.println("null");
			return null;
		}
		return list;	
	}


}
