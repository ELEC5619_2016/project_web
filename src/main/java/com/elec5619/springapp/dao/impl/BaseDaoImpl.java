package com.elec5619.springapp.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.springapp.dao.BaseDao;

@Transactional
@Repository
public class BaseDaoImpl<T> implements BaseDao<T>{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Resource
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}


	@Override
	public void add(T t){
		Session session = null;
		Transaction ts = null;
		try{
			session = getSession();
			ts = session.beginTransaction();
			session.save(t);
			ts.commit();
			
		}catch(Exception d){
			ts.rollback();
		}
	}

	@Override
	public void del(T t){
		Session session = null;
		Transaction ts = null;
		try{
			session = getSession();
			ts = session.beginTransaction();
			session.delete(t);
			ts.commit();
			
		}catch(Exception d){
			ts.rollback();
		}finally{
			session.close();
		}
	}
	@Override
	public void update(T t){
		Session session = null;
		Transaction ts = null;
		try{
			session = getSession();
			ts = session.beginTransaction();
			session.merge(t);
			ts.commit();
			
		}catch(Exception d){
			ts.rollback();
		}finally{
			session.close();
		}
	}

	@Override
	public List<T> queryAll(String hql){
		Session session = null;
		Transaction ts = null;
		List<T>list = null;
		try{
			session = getSession();
			ts = session.beginTransaction();
			Query query = session.createQuery(hql);
			list = query.list();
			ts.commit();
			
		}catch(Exception d){
			ts.rollback();
		}finally{
			session.close();
		}
		return list;
	}

	@Override
	public T queryOne(String hql, Integer id) {
		Session session = null;

		Transaction ts = null;
		List<T> list = null;
		try {
			session = getSession();
			ts = session.beginTransaction();
			Query query = session.createQuery("select u from " + hql
					+ " u where u.id=" + id);
			list = query.list();
			ts.commit();
		} catch (Exception d) {
			ts.rollback();
		} finally {
			session.close();
		}
		return list.get(0);
	}


	@Override
	public List<T> search(String table, String name) {

		Session session = null;

		Transaction ts = null;
		List<T> list = null;
		try {
			session = getSession();
			ts = session.beginTransaction();
			Query query = session.createQuery("select u from " + table
					+ " u where u.name like '%" + name + "%'");
			list = query.list();
			ts.commit();
		} catch (Exception d) {
			ts.rollback();
		} finally {
			session.close();
		}
		return list;
	}

	@Override
	public T queryName(String hql, String name) {
		Session session = null;

		Transaction ts = null;
		List<T> list = null;
		try {
			session = getSession();
			ts = session.beginTransaction();
			Query query = session.createQuery("select u from " + hql
					+ " u where u.name='" + name+"'");
			list = query.list();
			ts.commit();
		} catch (Exception d) {
			ts.rollback();
		} finally {
			session.close();
		}
		return list.get(0);
	}

}
