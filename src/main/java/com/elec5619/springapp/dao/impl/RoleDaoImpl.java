package com.elec5619.springapp.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.springapp.dao.RoleDao;
import com.elec5619.springapp.model.Role;

@Transactional
@Repository
public class RoleDaoImpl implements RoleDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Resource
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void save(Role role) {
		getSession().save(role);
	}

	@Override
	public void delete(Role role) {
		getSession().delete(role);
	}

	@Override
	public void update(Role role) {
		getSession().update(role);
	}

	@Override
	public Role findByUserid(long userid) {
		String hql = "from Role where userid=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, userid);
		List<Role> list = query.list();
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

}
