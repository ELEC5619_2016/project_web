package com.elec5619.springapp.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.springapp.dao.UserDao;
import com.elec5619.springapp.model.User;

@Transactional
@Repository
public class UserDaoImpl implements UserDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Resource
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void save(User user) {
		getSession().save(user);
	}

	@Override
	public void delete(long id) {
		User user = findById(id);
		getSession().delete(user);
	}

	@Override
	public void delete(User user) {
		getSession().delete(user);
	}

	@Override
	public void update(User user) {
		getSession().update(user);
	}

	@Override
	public User findById(long id) {
		return (User) getSession().get(User.class, id);
	}
	
	@Override
	public User findByEmail(String email) {
		String hql = "from User where email=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, email);
		List<User> list = query.list();
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public User findByUsernameAndPassword(String username, String password) {
		String hql = "from User where username=? and password=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, username);
		query.setParameter(1, password);
		List<User> list = query.list();
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public long findByUser(String username) {
		String hql = "from User where username=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, username);
		List<User> list = query.list();
		if (list == null || list.size() == 0) {
			return -1;
		}
		return list.get(0).getId();

	}
	
	@Override
	public List<User> findallusers() {
		String hql = "from User";
		Query query = getSession().createQuery(hql);
		List<User> list = query.list();
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}
	
	@Override
	public List<User> findByUsername(String username) {
		String hql = "from User where username=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, username);
		List<User> list = query.list();
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}

}
