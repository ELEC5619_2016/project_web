package com.elec5619.springapp.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.springapp.dao.AppDao;
import com.elec5619.springapp.model.AppUsage;
import com.elec5619.springapp.model.OffTime;

@Transactional
@Repository
public class AppDaoImpl implements AppDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Resource
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void save(AppUsage usage) {
		getSession().save(usage);
	}

	@Override
	public void delete(AppUsage usage) {
		getSession().delete(usage);
	}

	@Override
	public void update(AppUsage usage) {
		getSession().update(usage);
	}

	@Override
	public List<AppUsage> findByDate(String sdate, String edate, long uid) {
		String hql = "from AppUsage where date>=? and date<=? and user.id =?";
		Query query = getSession().createQuery(hql);
		if(sdate.indexOf("/")!= -1){
			sdate = sdate.replace("/", "-");
			edate = edate.replace("/", "-");
		}
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	    try {
			Date s_date = (Date) formatter.parse(sdate);
			Date e_date = (Date) formatter.parse(edate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(e_date);
			cal.add(Calendar.DATE, 1);
		
			System.out.println("sdate is:"+s_date);
	        System.out.println("edate is:"+cal.getTime());
			query.setParameter(0, s_date);
			query.setParameter(1, cal.getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		query.setParameter(2, uid);
		List<AppUsage> list = query.list();

		if (list == null || list.size() == 0) {
			System.out.println("null");
			return null;
		}
		System.out.println("app body=" + list.get(0).getAname());
		return list;	

	}

	@Override
	public AppUsage countApp(String name) {
		String hql = "from AppUsage where aname=? ";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, name);
		List<AppUsage> list = query.list();
		if (list == null || list.size() == 0) {
			System.out.println("false");
			return null;
		}
		return list.get(0);	
	}

	@Override
	public AppUsage findMostUsageByUId(long uid) {
		String hql = "from AppUsage as a where a.user.id =? and a.count=(select max(a.count) from AppUsage) ";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, uid);
		List<AppUsage> list = query.list();

		if (list == null || list.size() == 0) {
			System.out.println("null");
			return null;
		}
		return list.get(0);	
	}

}
