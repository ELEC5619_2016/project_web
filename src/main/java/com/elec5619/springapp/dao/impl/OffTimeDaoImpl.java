package com.elec5619.springapp.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

import com.elec5619.springapp.dao.OffTimeDao;
import com.elec5619.springapp.model.OffTime;
import com.elec5619.springapp.model.User;

@Transactional
@Repository
public class OffTimeDaoImpl implements OffTimeDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Resource
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void save(OffTime offtime) {
		getSession().save(offtime);
	}

	@Override
	public void delete(long id) {
		OffTime offtime = findById(id);
		getSession().delete(offtime);
		
	}

	@Override
	public void delete(OffTime offtime) {
		getSession().delete(offtime);
		
	}

	@Override
	public void update(OffTime offtime) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public OffTime findById(long id) {
		return (OffTime) getSession().get(OffTime.class, id);
	}
	

	@Override
	public List<OffTime> findByDate(String sdate, String edate, long uid) {
		String hql = "from OffTime where date>=? and date<=? and user.id =?";
		Query query = getSession().createQuery(hql);
		if(sdate.indexOf("/")!= -1){
			sdate = sdate.replace("/", "-");
			edate = edate.replace("/", "-");

		}

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	    try {
			Date s_date = (Date) formatter.parse(sdate);
			Date e_date = (Date) formatter.parse(edate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(e_date);
			cal.add(Calendar.DATE, 1);
		
			System.out.println("sdate is:"+s_date);
	        System.out.println("edate is:"+cal.getTime());
			query.setParameter(0, s_date);
			query.setParameter(1, cal.getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		query.setParameter(2, uid);
		List<OffTime> list = query.list();

		if (list == null || list.size() == 0) {
			System.out.println("null");
			return null;
		}
		return list;	

	}

	@Override
	public OffTime findMaxLengthByUId(long uid) {
		String hql = "from OffTime where user.id =? order by cast(length as int) DESC";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, uid);
		List<OffTime> list = query.list();

		if (list == null || list.size() == 0) {
			System.out.println("null");
			return null;
		}
		return list.get(0);	
	}

	@Override
	public OffTime findAppByUId(long uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<OffTime> findOfftimeByLength(String date) {
		String hql = "from OffTime where date>=? and date<=? group by user.id";
		Query query = getSession().createQuery(hql);
		if(date.indexOf("/")!= -1){
			date = date.replace("/", "-");
		}

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	    try {
	    	Date s_date = (Date) formatter.parse(date);
			Date e_date = (Date) formatter.parse(date);
			Calendar cal = Calendar.getInstance();
			cal.setTime(e_date);
			cal.add(Calendar.DATE, 1);
		
			System.out.println("sdate is:"+s_date);
	        System.out.println("edate is:"+e_date);
			query.setParameter(0, s_date);
			query.setParameter(1, cal.getTime());

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<OffTime> list = query.list();
		System.out.println(list);

		if (list == null || list.size() == 0) {
			System.out.println("null");
			return null;
		}
		return list;	
	}

	@Override
	public List<OffTime> findAll() {
		String hql = "from OffTime";
		Query query = getSession().createQuery(hql);
		List<OffTime> list = query.list();
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}

	@Override
	public OffTime findUserByDone() {
		String hql = "from OffTime group by user.id order by sum(cast(done as int)) DESC";
		Query query = getSession().createQuery(hql);
		List<OffTime> list = query.list();

		if (list == null || list.size() == 0) {
			System.out.println("null");
			return null;
		}
		return list.get(0);	
	
	}
}
