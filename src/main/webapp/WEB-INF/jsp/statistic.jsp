<%@ page contentType="text/html;charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%  
   String length = (String)request.getAttribute("length");
%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Phone Away - Phone Away Health Ahead</title>

	<link href=${pageContext.request.contextPath}/resources/jquery-3.1.1.min.js rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href=${pageContext.request.contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css rel="stylesheet">
    
    <!-- Bootstrap Core CSS -->
    <link href=${pageContext.request.contextPath}/resources/backend/vendor/bootstrap/css/bootstrap.min.css rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href=${pageContext.request.contextPath}/resources/backend/vendor/metisMenu/metisMenu.min.css rel="stylesheet">
   
    <!-- Custom CSS -->
    <link href=${pageContext.request.contextPath}/resources/backend/dist/css/sb-admin-2.css rel="stylesheet">
    <link href=${pageContext.request.contextPath}/resources/datetimepicker/jquery.datetimepicker.css rel="stylesheet" type="text/css" >
    
    <!-- Morris Charts CSS -->
    <link href=${pageContext.request.contextPath}/resources/backend/vendor/morrisjs/morris.css rel="stylesheet">

    <!-- Custom Fonts -->
    <link href=${pageContext.request.contextPath}/resources/backend/vendor/font-awesome/css/font-awesome.min.css rel="stylesheet" type="text/css">
  
    <!-- Custom Fonts -->
    <link href=${pageContext.request.contextPath}/resources/vendor/font-awesome/css/font-awesome.min.css rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href=${pageContext.request.contextPath}/resources/css/agency.min.css rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
   <style type="text/css">
	.custom-date-style {
		background-color: red !important;
	}
	.input{	
	}
	.input-wide{
		width: 500px;
	}
</style>

</head>


<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="/phoneaway">Phone Away</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
    					<a class="page-scroll" href="/phoneaway">Home</a>
                    </li>
                   <li>
                    	<c:if test="${!empty username}">
							<a class="page-scroll" href="/phoneaway/leaderboard">Dashboard</a>
						</c:if>        
                        
                    </li>
                    <li>
                        <a class="page-scroll" href="/phoneaway/achievement">Leaderboard</a>
                        
                    </li>
                    <li>
                    	<c:if test="${!empty username}">
							<a class="page-scroll" href="/phoneaway/step/statistic">Statistic</a>
						</c:if>          
                    </li>
                    <li>
                        <a class="page-scroll" href="/phoneaway/about">About us</a>
                    </li>
					<li>
						<!-- using JSTL -->
						<c:if test="${empty username}">
						<a class="btn btn-success" href="/phoneaway/login" id="loginbutton">Login/signup</a>
						</c:if>
						
						<c:if test="${!empty username}">
						<button class="btn btn-primary dropdown-toggle btn-lg" data-toggle="dropdown"><c:out value="${username}"/> 
								<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li><a href="/phoneaway/profile">profile</a></li>
							<li class="divider"></li>
							<li><a href="/phoneaway/login/logout">logout</a></li>
						</ul>
						</c:if>
					</li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
	
	<section>
	<div class="container">
		<div  class="row">
		<form:form id="date" class="form-horizontal" commandName="date" method="post">
				<input type="hidden" name="period" id="period"/>
             	<div style="width:200px;float:left">
            		<label >Start Date:</label >
					<input type="text" class="some_class" id="sdate" name="sdate" value="${sdate}"/>
			
           	 	</div>
            	<div style="width:200px;float:left;">
            		<label> End Date:</label >
					<input type="text" class="some_class" id="edate" name="edate" value="${edate}"/>
            	</div>
            	<div class="col-sm-12 controls" style="width:200px;float:left;clear:right">
                	<a id="btn-date" type="button" class="btn btn-success" onclick="query()">Today</a>
           		</div>
           		
           		<div class="col-sm-12 controls" style="width:200px;float:left;clear:right">
                	<a id="btn-date-week" type="button" class="btn btn-success" onclick="queryWeek()">Latest Week</a>
           		</div>
           		
           		<div class="col-sm-12 controls" style="width:200px;float:left;clear:right">
                	<a id="btn-date-month" type="button" class="btn btn-success" onclick="queryMonth()">Latest Month</a>
           		</div>
            </form:form>
            </div>
            <div  class="row">
		 <table class="table table-hover">
		 		<thead>
      				<tr>
      					<td></td>
         				<td style="font-weight:bold;">Date</td>
         				<td style="font-weight:bold;">Steps</td>
         				<td></td>
      				</tr>	
   				<thead> 
   				<c:if test="${fn:length(statistic) > 0}">
		 	<c:forEach var="item" items="${statistic}" varStatus="status">
		 		<tbody>
      				<tr>
      				    <td></td>
      					<td><fmt:formatDate value="${item.date}" type="date" pattern="yyyy-MM-dd"/></td>
         				<td>${item.steps}</td>
         				<td></td>
      				</tr>
      				
   				</tbody> 
			</c:forEach> 
			</c:if>
		 		 	
		 </table> 
		 </div>
		 </div>
	</section>


    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Phone Away Website 2016</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="/phoneaway/policy">Privacy Policy</a>
                        </li>
                        <li><a href="/phoneaway/termuse">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src=${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js></script>

    <!-- Bootstrap Core JavaScript -->
    <script src=${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.min.js></script>

    <!-- Contact Form JavaScript -->
    <script src=${pageContext.request.contextPath}/resources/js/jqBootstrapValidation.js></script>
    <script src=${pageContext.request.contextPath}/resources/js/contact_me.js></script>

    <!-- Theme JavaScript -->
    <script src=${pageContext.request.contextPath}/resources/js/agency.min.js></script>
    
     <!-- Metis Menu Plugin JavaScript -->
    <script src=${pageContext.request.contextPath}/resources/vendor/metisMenu/metisMenu.min.js></script>

    <!-- Custom Theme JavaScript -->
    <script src=${pageContext.request.contextPath}/resources/dist/js/sb-admin-2.js></script>
    <script src=${pageContext.request.contextPath}/resources/datetimepicker/jquery.js></script>
	<script src=${pageContext.request.contextPath}/resources/datetimepicker/build/jquery.datetimepicker.full.js></script>
	
	<script>
	$('#sdate').datetimepicker({
	      lang:"en",
	      format:"Y-m-d",
	      timepicker:false,
	      yearStart:2000,
	      yearEnd:2050,
	      todayButton:true
	});
	$('#edate').datetimepicker({
	      lang:"en",
	      format:"Y-m-d",
	      timepicker:false,
	      yearStart:2000,
	      yearEnd:2050,
	      todayButton:true
	});
	</script>
    
    <script type="text/javascript" src="resources/js/Chart.min.js"></script>
    
    <script type="text/javascript">
    var ctx = document.getElementById("myChart");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
            datasets: [{
                label: '# of Seconds',
                data: [<%=length%>],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    
  
   
    </script>
    
    <script type="text/javascript">
    function query() {
        var form = document.forms[0];
        form.action = "${pageContext.request.contextPath}/step/statistic/query";
        form.method = "post";
        form.submit();
    }
</script>

<script type="text/javascript">
    function queryWeek() {
        var form = document.forms[0];
        $("#sdate").attr("value",'');
        $("#edate").attr("value",'');
        $("#period").attr("value",'week');
        form.action = "${pageContext.request.contextPath}/step/statistic";
        form.method = "post";
        form.submit();
    }
</script>

<script type="text/javascript">
    function queryMonth() {
        var form = document.forms[0];
        $("#sdate").attr("value",'');
        $("#edate").attr("value",'');
        $("#period").attr("value",'month');
        form.action = "${pageContext.request.contextPath}/step/statistic";
        form.method = "post";
        form.submit();
    }
</script>

</body>

</html>
