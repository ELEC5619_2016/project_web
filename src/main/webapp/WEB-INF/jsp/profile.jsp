<%@ page contentType="text/html;charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Phone Away - Phone Away Health Ahead</title>

    <!-- Bootstrap Core CSS -->
    <link href="resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href="resources/css/agency.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="/phoneaway">Phone Away</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="/phoneaway">Home</a>
                    </li>
                  <li>
                    	<c:if test="${!empty username}">
							<a class="page-scroll" href="/phoneaway/leaderboard">Dashboard</a>
						</c:if>        
                        
                    </li>
                    <li>
                        <a class="page-scroll" href="/phoneaway/achievement">Leaderboard</a>
                        
                    </li>
                    <li>
                    	<c:if test="${!empty username}">
							<a class="page-scroll" href="/phoneaway/step/statistic">Statistic</a>
						</c:if>          
                    </li>
                    <li>
                        <a class="page-scroll" href="/phoneaway/about">About us</a>
                    </li>
					<li>
						<!-- using JSTL -->
						<c:if test="${empty username}">
						<a class="btn btn-success" href="/phoneaway/login" id="loginbutton">Login/signup</a>
						</c:if>
						
						<c:if test="${!empty username}">
						<button class="btn btn-primary dropdown-toggle btn-lg" data-toggle="dropdown"><c:out value="${username}"/> 
								<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li><a href="/phoneaway/profile">profile</a></li>
							<li class="divider"></li>
							<li><a href="/phoneaway/login/logout">logout</a></li>
						</ul>
						</c:if>
					</li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
	
	<section>

			<c:if test="${not empty user_update}">
				<tr>
					<td>
						<div class="alert alert-user-update" style="text-align:center">
							<strong>Update status: </strong>${user_update}
						</div>
					</td>
				</tr>
				<c:remove var="user_update" />
			</c:if>
			
			<c:if test="${not empty profile_update}">
				<tr>
					<td>
						<div class="alert alert-profile-update" style="text-align:center">
							<strong>Update status: </strong>${profile_update}
						</div>
					</td>
				</tr>
				<c:remove var="profile_update" />
			</c:if>

        <div style="margin-top:0px;"  class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    
                    <div class="panel-heading">
                        <div class="panel-title">User Account</div>
                    </div>

                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                        <form:form id="userform" class="form-horizontal" commandName="user" method="post" action="/phoneaway/login/updateuser" onsubmit="return validateuser()">
						<table>

							<div><label>* cannot be empty!</label></div>

                            <div class="form-group">
                                    <label for="username" class="col-md-3 control-label">Username</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static mb-0"><c:out value="${username}"></c:out></p>
                                    </div>
                            </div>

                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label">* Email</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="email" placeholder="Email Address" path="email" value="${user.email}"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">* Password</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="password" placeholder="Input password" path="password" value="${user.password}"/>
                                    </div>
                                </div>
								
                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">* Confirm Password</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="confirmPassword" placeholder="Input password again" value="${user.password}"/>
                                    </div>
                                </div>

								<div class="form-group">
                                    <div class="col-sm-12 controls" style="text-align: center">
                                    	<input id="btn-update" class="btn btn-success" type="submit" value="Update">
                                    </div>
                                </div>


						</table>
						</form:form>

					</div>                     
			</div>  
        </div>
        
        
        <div style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    
                    <div class="panel-heading">
                        <div class="panel-title">User Profile</div>
                    </div>

                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                        <form:form id="profileform" class="form-horizontal" commandName="profile" method="post" action="/phoneaway/profile/updateprofile" onsubmit="return validateprofile()">
						<table>

                            <div class="form-group">
                                    <label for="first_name" class="col-md-3 control-label">First name</label>
                                    <div class="col-md-9">
                                        <form:input type="text" class="form-control" name="first_name" path="first_name" value="${profile.first_name}"/>
                                    </div>
                            </div>

							<div class="form-group">
                                    <label for="last_name" class="col-md-3 control-label">Last name</label>
                                    <div class="col-md-9">
                                        <form:input type="text" class="form-control" name="last_name" path="last_name" value="${profile.last_name}"/>
                                    </div>
                            </div>

							<div class="form-group">
                                    <label for="gender" class="col-md-3 control-label">Gender</label>
									<div class="col-md-9">
                                        <form:select path="gender">
                                        	<option selected="selected" disabled="disabled">${profile.gender}</option>
											<form:option value="male">Male</form:option>
											<form:option value="female">Female</form:option>
										</form:select>
                                    </div>

                            </div>

							<div class="form-group">
                                    <label for="age" class="col-md-3 control-label">Age</label>
                                    <div class="col-md-9">
                                        <form:input type="text" class="form-control" name="age" path="age" value="${profile.age}"/>
                                    </div>
                            </div>

							<div class="form-group">
                                    <label for="country" class="col-md-3 control-label">Country</label>
                                    <div class="col-md-9">
                                        <form:input type="text" class="form-control" name="country" path="country" value="${profile.country}"/>
                                    </div>
                            </div>

							<div class="form-group">
                                    <label for="phonenumber" class="col-md-3 control-label">Phone</label>
                                    <div class="col-md-9">
                                        <form:input type="text" class="form-control" name="phonenumber" path="phone" value="${profile.phone}"/>
                                    </div>
                            </div>

								<div class="form-group">
                                    <div class="col-sm-12 controls" style="text-align: center">
                                    	<input id="btn-update" class="btn btn-success" type="submit" value="Update">
                                    </div>
                                </div>

                        </table>
                        </form:form>
					</div>                     
			</div>  
        </div>
        
	</section>

<script>

function validateprofile() {

    if(profileform.first_name.value.length!=0)  
    {
    	var x = profileform.first_name.value;
    	var valid = /^[A-Za-z]+$/;
    	if (valid.test(x)){
    	} else {
    		alert("Invalid first name!");
    		profileform.first_name.focus();
    		return false;
    	}
    }
    if(profileform.last_name.value.length!=0)  
    {  
    	var y = profileform.last_name.value;
    	var valid = /^[A-Za-z]+$/;
    	if (valid.test(y)){
    	} else {
    		alert("Invalid last name!");
    		profileform.last_name.focus();
    		return false;
    	}
    }
    
    if(profileform.age.value.length!=0)  
    {
    	var z = profileform.age.value;
    	var valid = /^\d+$/;
    	if (z.match(valid)){
    		if (z > 100 || z < 0) {
    			alert("Invalid age!");
    			profileform.age.focus();
    			return false;
    		}
    	} else {
    		alert("Age should be number!");
    		profileform.age.focus();
    		return false;
    	}
    }
    if(profileform.country.value.length!=0)  
    {  
    	var n = profileform.country.value;
    	var valid = /^[A-Za-z ]+$/;
    	if (valid.test(n)){
    	} else {
    		alert("Invalid country!");
    		profileform.country.focus();
    		return false;
    	}
    }
    if(profileform.phone.value.length!=0)  
    {
    	var m=profileform.phone.value;
    	var valid = /^\d{3,16}$/;
    	if (valid.test(m)){
    	} else {
    		alert("Invalid phone numbers!");
    		profileform.phone.focus();
    		return false;
    	}
    }
}

function validateuser() {

    if(userform.email.value.length!=0)  
    {  
    	var x=userform.email.value;
    	var atpos=x.indexOf("@");
    	var dotpos=x.lastIndexOf(".");
    	if (atpos<1 || dotpos<atpos+2 || dotpos+2>x.length || dotpos+2==x.length){
    		alert("Invalid email address!");
    		userform.email.focus();
    		return false;
    	}
    }  
    else  
    {  
        alert("Email cannot be empty, please type email!");  
        userform.email.focus();  
        return false;  
    }
    if(userform.password.value=="")  
    {  
         alert("Password cannot be empty, please type password!");  
         userform.password.focus();  
         return false;  
    }  
    if(userform.confirmPassword.value=="")  
    {  
         alert("Confirm password cannot be empty, please type confirm password!");  
         userform.confirmPassword.focus();  
         return false;  
    }  
    if(userform.password.value!=signupform.confirmPassword.value)  
    {  
        alert("Password is different with comfirm password!");  
        userform.confirmPassword.focus();  
        return false;  
    }
}

</script>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Phone Away Website 2016</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="https://twitter.com/?lang=en"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="https://en-gb.facebook.com/"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="https://au.linkedin.com/"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="/phoneaway/policy">Privacy Policy</a>
                        </li>
                        <li><a href="/phoneaway/termuse">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="resources/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="resources/js/jqBootstrapValidation.js"></script>
    <script src="resources/js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="resources/js/agency.min.js"></script>

</body>

</html>

