<%@ page contentType="text/html;charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Phone Away - Phone Away Health Ahead</title>

    <link href=${pageContext.request.contextPath}/resources/jquery-3.1.1.min.js rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href=${pageContext.request.contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css rel="stylesheet">
    
    <!-- Bootstrap Core CSS -->
    <link href=${pageContext.request.contextPath}/resources/backend/vendor/bootstrap/css/bootstrap.min.css rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href=${pageContext.request.contextPath}/resources/backend/vendor/metisMenu/metisMenu.min.css rel="stylesheet">
   
    <!-- Custom CSS -->
    <link href=${pageContext.request.contextPath}/resources/backend/dist/css/sb-admin-2.css rel="stylesheet">
    <link href=${pageContext.request.contextPath}/resources/datetimepicker/jquery.datetimepicker.css rel="stylesheet" type="text/css" >
    
    <!-- Morris Charts CSS -->
    <link href=${pageContext.request.contextPath}/resources/backend/vendor/morrisjs/morris.css rel="stylesheet">

    <!-- Custom Fonts -->
    <link href=${pageContext.request.contextPath}/resources/backend/vendor/font-awesome/css/font-awesome.min.css rel="stylesheet" type="text/css">
  
    <!-- Custom Fonts -->
    <link href=${pageContext.request.contextPath}/resources/vendor/font-awesome/css/font-awesome.min.css rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href=${pageContext.request.contextPath}/resources/css/agency.min.css rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.html">Phone Away</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
    					<a class="page-scroll" href="/phoneaway">Home</a>
                    </li>
                    <li>
                    	<c:if test="${!empty username}">
							<a class="page-scroll" href="/phoneaway/leaderboard">Dashboard</a>
						</c:if>        
                        
                    </li>
                    <li>
                        <a class="page-scroll" href="/phoneaway/achievement">Leaderboard</a>
                        
                    </li>
                    <li>
                    	<c:if test="${!empty username}">
							<a class="page-scroll" href="/phoneaway/step/statistic">Statistic</a>
						</c:if>          
                    </li>
                    <li>
                        <a class="page-scroll" href="/phoneaway/about">About us</a>
                    </li>
					<li>
						<!-- using JSTL -->
						<c:if test="${empty username}">
						<a class="btn btn-success" href="/phoneaway/login" id="loginbutton">Login/signup</a>
						</c:if>
						
						<c:if test="${!empty username}">
						<button class="btn btn-primary dropdown-toggle btn-lg" data-toggle="dropdown"><c:out value="${username}"/> 
								<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li><a href="/phoneaway/profile">profile</a></li>
							<li class="divider"></li>
							<li><a href="/phoneaway/login/logout">logout</a></li>
						</ul>
						</c:if>
					</li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
	


<section>
   <form:form id="date" class="form-horizontal" commandName="date" method="post">
   		 <div style="width:200px;float:left">   
			<input type="text" class="some_class" value="" id="some_class_1" name="date" />	
   		</div>
   
   		<div class="col-sm-12 controls" style="width:200px;float:left;clear:right">
       		<a id="btn-date" type="button" class="btn btn-success" onclick="queryA()">Search</a>
   		</div>
   </form:form>
 	<div>
        <table class="table table-striped">
            <caption>Time Ranking</caption>
            <thead>
                <tr>
                    <th>Rank</th>
                    <th>Name</th>

                    <th>TotalTime/sec</th>
                </tr>
            </thead>
            <tbody>
      			<c:forEach items="${achievementList}" var="item" varStatus="status">
      				<tr>
      					<th><c:out value="${status.count}"/></th>
                    	<th><c:out value="${item.user.username}"/></th>

                    	<th><c:out value="${item.length}"/></th>
      				</tr>
    			</c:forEach>
		
               
            </tbody>
        </table>
        </div>

    <div >
        <table class="table table-striped">
            <caption>Steps Ranking</caption>
            <thead>
                <tr>
                    <th>Rank</th>
                    <th>Name</th>

                    <th>TotalSteps</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${stepList}" var="item" varStatus="status">
      				<tr>
      					<th><c:out value="${status.count}"/></th>
                    	<th><c:out value="${item.user.username}"/></th>

                    	<th><c:out value="${item.steps}"/></th>
      				</tr>
    			</c:forEach>
                
            </tbody>
        </table>
        </div>
</section>




    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Phone Away Website 2016</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="/phoneaway/policy">Privacy Policy</a>
                        </li>
                        <li><a href="/phoneaway/termuse">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="resource/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resource/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="resource/js/jqBootstrapValidation.js"></script>
    <script src="resource/js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="resource/js/agency.min.js"></script>
    
     <!-- Custom Theme JavaScript -->
    <script src=${pageContext.request.contextPath}/resources/datetimepicker/jquery.js></script>
	<script src=${pageContext.request.contextPath}/resources/datetimepicker/build/jquery.datetimepicker.full.js></script>
    <script>
	$.datetimepicker.setLocale('en');

	$('#datetimepicker_format').datetimepicker({value:'2015/04/15 05:03', format: $("#datetimepicker_format_value").val()});
	console.log($('#datetimepicker_format').datetimepicker('getValue'));

	$("#datetimepicker_format_change").on("click", function(e){
	$("#datetimepicker_format").data('xdsoft_datetimepicker').setOptions({format: $("#datetimepicker_format_value").val()});
	});
	$("#datetimepicker_format_locale").on("change", function(e){
	$.datetimepicker.setLocale($(e.currentTarget).val());
	});

	$('#datetimepicker').datetimepicker({
	dayOfWeekStart : 1,
	lang:'en',
	disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
	startDate:	'1986/01/05'
	});
	$('#datetimepicker').datetimepicker({value:'2015/04/15 05:03',step:10});

	$('.some_class').datetimepicker();

	$('#default_datetimepicker').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	//defaultDate:'8.12.1986', // it's my birthday
	defaultDate:'+03.01.1970', // it's my birthday
	defaultTime:'10:00',
	timepickerScrollbar:false
	});

	$('#datetimepicker10').datetimepicker({
	step:5,
	inline:true
	});
	$('#datetimepicker_mask').datetimepicker({
	mask:'9999/19/39 29:59'
	});

	$('#datetimepicker1').datetimepicker({
	datepicker:false,
	format:'H:i',
	step:5
	});
	$('#datetimepicker2').datetimepicker({
	yearOffset:222,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	minDate:'-1970/01/02', // yesterday is minimum date
	maxDate:'+1970/01/02' // and tommorow is maximum date calendar
	});
	$('#datetimepicker3').datetimepicker({
	inline:true
	});
	$('#datetimepicker4').datetimepicker();
	$('#open').click(function(){
	$('#datetimepicker4').datetimepicker('show');
	});
	$('#close').click(function(){
	$('#datetimepicker4').datetimepicker('hide');
	});
	$('#reset').click(function(){
	$('#datetimepicker4').datetimepicker('reset');
	});
	$('#datetimepicker5').datetimepicker({
	datepicker:false,
	allowTimes:['12:00','13:00','15:00','17:00','17:05','17:20','19:00','20:00'],
	step:5
	});
	$('#datetimepicker6').datetimepicker();
	$('#destroy').click(function(){
	if( $('#datetimepicker6').data('xdsoft_datetimepicker') ){
		$('#datetimepicker6').datetimepicker('destroy');
		this.value = 'create';
	}else{
		$('#datetimepicker6').datetimepicker();
		this.value = 'destroy';
	}
	});
	var logic = function( currentDateTime ){
	if (currentDateTime && currentDateTime.getDay() == 6){
		this.setOptions({
			minTime:'11:00'
		});
	}else
		this.setOptions({
			minTime:'8:00'
		});
	};
	$('#datetimepicker7').datetimepicker({
	onChangeDateTime:logic,
	onShow:logic
	});
	$('#datetimepicker8').datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date')
			.toggleClass('xdsoft_disabled');
	},
	minDate:'-1970/01/2',
	maxDate:'+1970/01/2',
	timepicker:false
	});
	$('#datetimepicker9').datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date.xdsoft_weekend')
			.addClass('xdsoft_disabled');
	},
	weekends:['01.01.2014','02.01.2014','03.01.2014','04.01.2014','05.01.2014','06.01.2014'],
	timepicker:false
	});
	var dateToDisable = new Date();
	dateToDisable.setDate(dateToDisable.getDate() + 2);
	$('#datetimepicker11').datetimepicker({
	beforeShowDay: function(date) {
		if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
			return [false, ""]
		}

		return [true, ""];
	}
	});
	$('#datetimepicker12').datetimepicker({
	beforeShowDay: function(date) {
		if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
			return [true, "custom-date-style"];
		}

		return [true, ""];
	}
	});
	$('#datetimepicker_dark').datetimepicker({theme:'dark'})

	</script>
	
	 <script type="text/javascript">
    function queryA() {
        var form = document.forms[0];
        form.action = "${pageContext.request.contextPath}/achievement";
        form.method = "post";
        form.submit();
        
    }
	</script>
	
	   <!-- jQuery -->
    <script src=${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js></script>

    <!-- Bootstrap Core JavaScript -->
    <script src=${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.min.js></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src=${pageContext.request.contextPath}/resources/js/jqBootstrapValidation.js></script>
    <script src=${pageContext.request.contextPath}/resources/js/contact_me.js></script>
    


</body>

</html>
