<%@ page contentType="text/html;charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
<head>
<title>Phone Away - Phone Away Health Ahead</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">
 <link href="resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Quattrocento+Sans' rel='stylesheet' type='text/css'>
 <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
 <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
 <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
 <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

 <!-- Theme CSS -->

<link href="css/agency.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css">

.btn_submit,.btn_cancel {
    width: 80px; height: 30px; BORDER-RIGHT: #2C59AA 1px solid; PADDING-RIGHT: 2px; BORDER-TOP: #2C59AA 1px solid; PADDING-LEFT: 2px; FONT-SIZE: 12px; FILTER: progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr=#ffffff, EndColorStr=#C3DAF5); BORDER-LEFT: #2C59AA 1px solid; CURSOR: hand; COLOR: black; PADDING-TOP: 2px; BORDER-BOTTOM: #2C59AA 1px solid;margin-right: 50px; 
}

.t {
    BORDER-RIGHT: #a6cbe7 1px solid; BORDER-TOP: #a6cbe7 1px solid; MARGIN: 0px auto 8px; BORDER-LEFT: #a6cbe7 1px solid; BORDER-BOTTOM: #a6cbe7 1px solid;width: 60%;
}
.t TABLE {
    BORDER-RIGHT: #fff 1px solid; BORDER-TOP: #fff 1px solid; MARGIN: 0px auto; BORDER-LEFT: #fff 1px solid; WIDTH:100%; BORDER-BOTTOM: #fff 1px solid
}
.h {
    PADDING-RIGHT: 7px; PADDING-LEFT: 7px; BACKGROUND: #e0f0f9; PADDING-BOTTOM: 1px; COLOR: #004c7d; PADDING-TOP: 1px; BORDER-BOTTOM: #dbecf4 4px solid; TEXT-ALIGN: left;font-size: 20px; height: 50px
}
.tr3 TD {
    PADDING-RIGHT: 8px; PADDING-LEFT: 8px; PADDING-BOTTOM: 10px; PADDING-TOP: 10px; BORDER-BOTTOM: #d4eff7 1px solid; BORDER-RIGHT: #d4eff7 1px solid
}
.tr3 TH {
    PADDING-RIGHT: 8px; PADDING-LEFT: 8px; PADDING-BOTTOM: 10px; PADDING-TOP: 10px; BORDER-BOTTOM: #d4eff7 1px solid; FONT-WEIGHT: normal; TEXT-ALIGN: left; BORDER-RIGHT: #d4eff7 1px solid
}
TEXTAREA {
    BORDER-RIGHT: #a6cbe7 1px solid; BORDER-TOP: #a6cbe7 1px solid; BORDER-LEFT: #a6cbe7 1px solid; BORDER-BOTTOM: #a6cbe7 1px solid
}
.input {
    BORDER-RIGHT: #a6cbe7 1px solid; PADDING-RIGHT: 0px; BORDER-TOP: #a6cbe7 1px solid; PADDING-LEFT: 1px; FONT-SIZE: 1em; PADDING-BOTTOM: 2px; VERTICAL-ALIGN: middle; BORDER-LEFT: #a6cbe7 1px solid; PADDING-TOP: 2px; BORDER-BOTTOM: #a6cbe7 1px solid
}
</style>
</HEAD>

<BODY id="page-top" class="index">

    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> 
    <script src="http://libs.baidu.com/jquery/1.9.0/jquery.min.js"></script>
    
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="/phoneaway">Phone Away</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="/phoneaway">Home</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/phoneaway/leaderboard">LeaderBoard</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/phoneaway/about">About us</a>
                    </li>
                    <li>
                        <a class="btn btn-success" href="/phoneaway/login">Login/signup</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

<!--main-->
    <DIV style="margin-top:50px;">
        <form name="postForm" action="/phoneaway/community/create" commandName="topic" method="POST"> 
            <DIV class="t">
                <TABLE cellSpacing="0" cellPadding="0" align="center">
                    <TR>
                        <TD class="h" colSpan="3"><b>Create a new topic</b></TD>
                    </TR>
    
                    <TR class="tr3">
                        <TH style="text-align:center;" width="15%;padding-top:10px;"><B>Title</B></TH>
                        <TH><INPUT class="input" style="PADDING-LEFT: 2px; FONT: 14px Tahoma;height:30px; width:500px;margin-left:10px;" tabIndex="1" size="60" name="title"></TH>
                    </TR>
    
                    <TR class="tr3">
                        <TH style="text-align:center;" vAlign=top>
                          <DIV><B>Content</B></DIV>
                        </TH>
                        <TH colSpan=2>
                            <DIV>   
                                <span><textarea style="WIDTH: 500px;margin-left:10px;" name="content" rows="20" cols="90" tabIndex="2" ></textarea></span>
                            </DIV>
                            <br/>
                            <DIV style="MARGIN: 15px 0px; width:500px; TEXT-ALIGN: center;">
                              <INPUT class="btn btn-success" style="margin-right:50px;" tabIndex="3" type="submit" value="Submit"> 
                              <INPUT class="btn btn-success" tabIndex="4" type="reset"  value="Cancel">
                            </DIV>
                        </TH>
                    </TR>
                </TABLE>
            </DIV>      
    
            
        </FORM> 
    </DIV>
</DIV>

<footer>
        <div class="container" style="padding-top:20px;border-top:1px solid;">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Phone Away Website 2016</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="#">Privacy Policy</a>
                        </li>
                        <li><a href="#">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

<!-- jQuery -->
 <script src="resources/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="resources/js/jqBootstrapValidation.js"></script>
<script src="resources/js/contact_me.js"></script>

<!-- Theme JavaScript -->
<script src="resources/js/agency.min.js"></script>

</BODY>
</HTML>
