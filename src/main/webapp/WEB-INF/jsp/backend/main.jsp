<%@ page contentType="text/html;charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Phoneaway Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="<%=path%>/resources/backend/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<%=path%>/resources/backend/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%=path%>/resources/backend/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<%=path%>/resources/backend/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                </button>
                <a class="navbar-brand">Phoneaway Admin</a>
            </div>
            <!-- /.navbar-header -->

               <ul class="nav navbar-top-links navbar-right">
					<li>
						<a class="navbar-big"><c:out value="${user.username}"/></a>
					</li>
					<li>
						<a class="btn btn-success btn-lg" href="/phoneaway/backend/logout" type="button">logout</a>
					</li>
                </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="/phoneaway/backend/main"><i class="fa fa-userlist fa-fw"></i> User Management</a>
                        </li>
                        
                         <li>
                            <a href="/phoneaway/backend/data"><i class="fa fa-userlist fa-fw"></i> Data Management</a>
                        </li>
                        
                    </ul>
                </div>
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User List</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                <div class="col-lg-15">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> All users

                            <form:form id="searchform" class="form-horizontal" commandName="user" action="/phoneaway/backend/search" method="post">
                        		<div class="input-group custom-search-form">
                                <form:input type="text" class="form-control" placeholder="Search by username" path="username"/>
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button" onclick="document.getElementById('searchform').submit()">
                                    <i class="fa fa-search"></i>
                                </button>
                            	</span>
                        		</div>
                            </form:form>

			<!-- for fail login -->
			<c:if test="${not empty search_fail}">
				<tr>
					<td>
						<div class="alert alert-search-fail">
							<strong>${search_fail}</strong>
						</div>
					</td>
				</tr>
				<c:remove var="search_fail" />
			</c:if>

                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">

			<c:if test="${not empty user_update}">
				<tr>
					<td>
						<div class="alert alert-user-update" style="text-align:center">
							<strong>Update status: </strong>${user_update}
						</div>
					</td>
				</tr>
				<c:remove var="user_update" />
			</c:if>

			<c:if test="${not empty user_delete}">
				<tr>
					<td>
						<div class="alert alert-user-delete" style="text-align:center">
							<strong>Delete status: </strong>${user_delete}
						</div>
					</td>
				</tr>
				<c:remove var="user_delete" />
			</c:if>

			<c:if test="${not empty user_add}">
				<tr>
					<td>
						<div class="alert alert-user-add" style="text-align:center">
							<strong>Add status: </strong>${user_add}
						</div>
					</td>
				</tr>
				<c:remove var="user_add" />
			</c:if>

							<table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
         											<td style="font-weight:bold;">Username</td>
         											<td style="font-weight:bold;">Email</td>
         											<td style="font-weight:bold;">Password</td>
         											<td style="font-weight:bold;"></td>
                                                </tr>
                                            </thead>
                                            <tbody>
												<form:form id="newform" commandName="user" action="/phoneaway/backend/modify" method="post" onsubmit="return validateadd()">
										      		<tr>
										      			<td><form:input path="username" placeholder="Input username"/></td>
										         		<td><form:input path="email" placeholder="Input email address"/></td>
										         		<td><form:input path="password" placeholder="Input password"/></td>
										         		<td><input class="btn btn-success" name="action" type="submit" value="Add User"/></td>
										      		</tr>
										      	</form:form>
                                            </tbody>
                                        </table>

                               <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <td style="font-weight:bold;">Id</td>
         											<td style="font-weight:bold;">Username</td>
         											<td style="font-weight:bold;">Email</td>
         											<td style="font-weight:bold;">Password</td>
         											<td style="font-weight:bold;">Create date</td>
         											<td style="font-weight:bold;"></td>
         											<td style="font-weight:bold;"></td>
                                                </tr>
                                            </thead>
                                            <tbody>
										   		<c:if test="${fn:length(userlist) > 0}">
												 	<c:forEach var="item" items="${userlist}" varStatus="status">
												 		<tbody>
												 		<form:form id="listform" commandName="user" action="/phoneaway/backend/modify" method="post" onsubmit="return validatemodify()">
										      				<tr>
										      				    <td>${item.id}</td>
										      					<td><form:input path="username" value="${item.username}"/></td>
										         				<td><form:input path="email" value="${item.email}"/></td>
										         				<td><form:input path="password" value="${item.password}"/></td>
										         				<td>${item.create_date}</td>
										         				<td><input class="btn btn-info" name="action" type="submit" value="Update"/></td>
										         				<td><input class="btn btn-danger" name="action" type="submit" value="Delete"/></td>
										      				</tr>
										      			</form:form>
										   				</tbody>
													</c:forEach>
												</c:if>
                                            </tbody>
                                        </table>
                                    <!-- /.table-responsive -->
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                </div>
                <!-- /.col-lg-8 -->

            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

<script>

function validateadd() {
	if(newform.username.value=="")  
    {  
         alert("Username cannot be empty, please type username!");  
         newform.username.focus();  
         return false;  
    }
    if(newform.email.value.length!=0)  
    {  
    	var x=listform.email.value;
    	var atpos=x.indexOf("@");
    	var dotpos=x.lastIndexOf(".");
    	if (atpos<1 || dotpos<atpos+2 || dotpos+2>x.length || dotpos+2==x.length){
    		alert("Invalid email address!");
    		newform.email.focus(); 
    		return false;
    	}
    }  
    else  
    {  
        alert("Email cannot be empty, please type email!");  
        newform.email.focus();  
        return false;  
    }
    if(newform.password.value=="")  
    {  
         alert("Password cannot be empty, please type password!");  
         newform.password.focus();  
         return false;  
    }
}

function validatemodify() {
	if(listform.username.value=="")  
    {  
         alert("Username cannot be empty, please type username!");  
         listform.username.focus();  
         return false;  
    }
    if(listform.email.value.length!=0)  
    {  
    	var x=listform.email.value;
    	var atpos=x.indexOf("@");
    	var dotpos=x.lastIndexOf(".");
    	if (atpos<1 || dotpos<atpos+2 || dotpos+2>x.length || dotpos+2==x.length){
    		alert("Invalid email address!");
    		listform.email.focus(); 
    		return false;
    	}
    }  
    else  
    {  
        alert("Email cannot be empty, please type email!");  
        listform.email.focus();  
        return false;  
    }
    if(listform.password.value=="")  
    {  
         alert("Password cannot be empty, please type password!");  
         listform.password.focus();  
         return false;  
    }
}

</script>


    <!-- jQuery -->
    <script src="<%=path%>/resources/backend/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<%=path%>/resources/backend/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<%=path%>/resources/backend/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<%=path%>/resources/backend/vendor/raphael/raphael.min.js"></script>
    <script src="<%=path%>/resources/backend/vendor/morrisjs/morris.min.js"></script>
    <script src="<%=path%>/resources/backend/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<%=path%>/resources/backend/dist/js/sb-admin-2.js"></script>

</body>

</html>