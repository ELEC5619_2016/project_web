<%@ page contentType="text/html;charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Phoneaway Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="<%=path%>/resources/backend/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<%=path%>/resources/backend/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%=path%>/resources/backend/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<%=path%>/resources/backend/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading" style="text-align: center">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>

                    <div class="panel-body">
                        <form:form id="adminform" commandName="user" method="post" action="/phoneaway/backend">
                            <table>

			<!-- for fail login -->
			<c:if test="${not empty login_error}">
				<tr>
					<td>
						<div class="alert alert-error">
							<strong>Login fail: </strong>${login_error}
						</div>
					</td>
				</tr>
				<c:remove var="login_error" />
			</c:if>

                                <div class="form-group">
                                    <form:input type="text" class="form-control" name="username" placeholder="username" path="username"/>
                                </div>
                                <div class="form-group">
                                    <form:input type="password" class="form-control" name="password" placeholder="password" path="password"/>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <div style="text-align: center">
                                	<a id="btn-login" class="btn btn-success" onclick="document.getElementById('adminform').submit()">Login</a>
                                </div>
                            </table>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="<%=path%>/resources/backend/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<%=path%>/resources/backend/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<%=path%>/resources/backend/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<%=path%>/resources/backend/dist/js/sb-admin-2.js"></script>

</body>

</html>
