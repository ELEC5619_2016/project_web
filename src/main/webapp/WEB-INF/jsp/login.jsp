<%@ page contentType="text/html;charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Phone Away - Phone Away Health Ahead</title>

    <!-- Bootstrap Core CSS -->
    <link href="resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href="resources/css/agency.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="/phoneaway">Phone Away</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="/phoneaway">Home</a>
                    </li>
                    <li>
                    	<c:if test="${!empty username}">
							<a class="page-scroll" href="/phoneaway/leaderboard">Dashboard</a>
						</c:if>        
                        
                    </li>
                    <li>
                        <a class="page-scroll" href="/phoneaway/achievement">Leaderboard</a>
                        
                    </li>
                    <li>
                    	<c:if test="${!empty username}">
							<a class="page-scroll" href="/phoneaway/step/statistic">Statistic</a>
						</c:if>          
                    </li>
                    <li>
                        <a class="page-scroll" href="/phoneaway/about">About us</a>
                    </li>
					<li>
						<a class="btn btn-success" href="/phoneaway/login">Login/signup</a>
					</li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
	
	<section>

		<div style="text-align:center">
			<c:if test="${not empty signup_fail}">
				<tr>
					<td>
						<div class="alert alert-signup-fail">
							<strong>Signup fail: </strong>${signup_fail}
						</div>
					</td>
				</tr>
				<c:remove var="signup_fail" />
			</c:if>
		</div>

        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title">Sign In</div>
                        <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div>
                    </div>     

                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                        <form:form id="loginform" class="form-horizontal" commandName="user" method="post">
						<table>    
                        
			<!-- for fail login -->
			<c:if test="${not empty login_error}">
				<tr>
					<td>
						<div class="alert alert-error">
							<strong>Login fail: </strong>${login_error}
						</div>
					</td>
				</tr>
				<c:remove var="login_error" />
			</c:if>

                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <form:input type="text" class="form-control" name="username" placeholder="username" path="username"/>
                                    </div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <form:input type="password" class="form-control" name="password" placeholder="password" path="password"/>
                                    </div>

                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->
                                    <div class="col-sm-12 controls" style="text-align: center">
                                      <a id="btn-login" class="btn btn-success" onclick="document.getElementById('loginform').submit()">Login</a>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                            Don't have an account?
                                        <a onClick="$('#loginbox').hide(); $('#signupbox').show()">
                                            Sign Up Here
                                        </a>
                                        </div>
                                    </div>
                                </div>
                                        </table>
                           </form:form>
					</div>                     
			</div>  
        </div>

        <div id="signupbox" style="display:none" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title">Sign Up</div>
                            <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" onclick="$('#signupbox').hide(); $('#loginbox').show()">Sign In</a></div>
                        </div>  
                        <div class="panel-body" >
                            <form:form name="signupform" class="form-horizontal" commandName="user" action="/phoneaway/login/signup" method="post" onsubmit="return validate()">
                            <table>
                                
                                <div><label>* cannot be empty!</label></div>
								
                                <div class="form-group">
                                    <label for="username" class="col-md-3 control-label">* Username</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="username" placeholder="Username" path="username"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label">* Email</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="email" placeholder="Email Address" path="email"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">* Password</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="password" placeholder="Input password" path="password"/>
                                    </div>
                                </div>
								
                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">* Confirm Password</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="confirmPassword" placeholder="Input password again"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 controls" style="text-align: center">
                                    	<input id="btn-signup" class="btn btn-info" type="submit" value="Sign Up">
                                    </div>
                                </div>
                            </table>
                            </form:form>
                         </div>
                    </div>

<script>
    function validate() {
    	if(signupform.username.value=="")  
        {  
             alert("Username cannot be empty, please type username!");  
             signupform.username.focus();  
             return false;  
        }
        if(signupform.email.value.length!=0)  
        {  
        	var x=signupform.email.value;
        	var atpos=x.indexOf("@");
        	var dotpos=x.lastIndexOf(".");
        	if (atpos<1 || dotpos<atpos+2 || dotpos+2>x.length || dotpos+2==x.length){
        		alert("Invalid email address!");
        		signupform.email.focus(); 
        		return false;
        	}
        }  
        else  
        {  
            alert("Please type email!");  
            signupform.email.focus();  
            return false;  
        }
        if(signupform.password.value=="")  
        {  
             alert("Password cannot be empty, please type password!");  
             signupform.password.focus();  
             return false;  
        }  
        if(signupform.confirmPassword.value=="")  
        {  
             alert("Confirm password cannot be empty, please type confirm password!");  
             signupform.confirmPassword.focus();  
             return false;  
        }  
        if(signupform.password.value!=signupform.confirmPassword.value)  
        {  
            alert("Password is different with comfirm password!");  
            signupform.confirmPassword.focus();  
            return false;  
        }
    }

</script>
               
               
                
         </div>
		
	</section>


    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Phone Away Website 2016</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="https://twitter.com/?lang=en"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="https://en-gb.facebook.com/"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="https://au.linkedin.com/"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="/phoneaway/policy">Privacy Policy</a>
                        </li>
                        <li><a href="/phoneaway/termuse">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="resources/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="resources/js/jqBootstrapValidation.js"></script>
    <script src="resources/js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="resources/js/agency.min.js"></script>

</body>

</html>