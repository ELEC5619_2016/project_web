<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML>
<html>
<head>
<title>Phone Away - Phone Away Health Ahead</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">
 <link href="resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Quattrocento+Sans' rel='stylesheet' type='text/css'>
 <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
 <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
 <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
 <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

 <!-- Theme CSS -->

<link href="css/agency.min.css" rel="stylesheet">
<link href="css/style2.css" rel="stylesheet" type="text/css" media="all" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body  id="page-top" class="index">
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> 
    <script src="http://libs.baidu.com/jquery/1.9.0/jquery.min.js"></script>

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="/phoneaway">Phone Away</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="/phoneaway">Home</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/phoneaway/leaderboard">LeaderBoard</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/phoneaway/about">About us</a>
                    </li>
                    <li>
                        <a class="btn btn-success" href="/phoneaway/login">Login/signup</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    
<!--main-->
<div class="main" style="margin-top:15px;">
        <TABLE style="border-style: outset; BORDER-TOP-WIDTH: -3px; BORDER-BOTTOM-WIDTH: 0px; TABLE-LAYOUT: fixed;margin-top:5px;" width="100%">
            <TR class="tr1">
             <TD bgcolor="#e0f0f9" style="text-align:left;vertical-align:middle;padding:5px;padding-left:20px;padding-top:10px;" colspan="2" height=45>
               <h3>Topic: ${topic.title }</h3><BR/>
             </TD>
        </TABLE>
        <c:if test="${cpage eq '1'}">
        <TABLE style="border-style: solid; BORDER-BOTTOM-WIDTH: 1px; TABLE-LAYOUT: fixed"  class="table table-striped" width="100%">
             <TR class="tr1">
                <TD style="WIDTH: 20%" bgcolor="#eff4fb" valign="top" align="center">
                    <B>${topic.user.username }</B><BR/><BR/>
                    <img src="img/User.jpg" style="height: 117px; width: 117px; "/><BR/>
                    Thread starter<br><BR/>
                    <font size="2">posted:<br>${topic.postdate }</font>
                </TD>
                <TD style="WIDTH: 80%" height=300>
                    <BR/>&nbsp;
                    <pre>&nbsp;&nbsp;${topic.content }</pre><BR/>
                </TD>
            </TR><TR class="tr1">
            <TD  height=20 bgcolor="#eff4fb">
            </TD>
            <TD align="right">
            <c:if test="${not empty user}">
                 <a href="TopicDetails.jsp#InputComment">Reply</a>
             </c:if>
            </TD>
            </TR>
        </TABLE>
        </c:if>
        <BR/>
        <c:forEach var="list" items="${commlist}">
        <TABLE style="border-style: solid; BORDER-BOTTOM-WIDTH: 1px; TABLE-LAYOUT: fixed"  class="table table-striped"  width="100%">
            <TR class="tr1">
                <TD style="WIDTH: 20%" bgcolor="#eff4fb" valign="top" align="center">
                    <B>${list.user.username }</B><BR/>
                    <img src="img/User.jpg" style="height: 117px; width: 117px;"/><BR/><BR/>
                    <DIV class="tipad gray">
                        <font size="2"> commented: <br> ${list.commentdate }</font>
                    </DIV>
                </TD>
                <TD style="WIDTH: 80%" height=300>
                    <BR/>&nbsp;&nbsp;&nbsp;&nbsp;
                    <p class="text-left">&nbsp;&nbsp;${list.content }</p><BR/>
                </TD>
            </TR>
            <c:if test="${not empty user}">
            <TR class="tr1">
            <TD  height=20 bgcolor="#eff4fb">
            </TD>
            <TD align="right">
             <c:if test="${list.users.id eq user.id}">
              <A href="/phoneaway/community/delete"><font color="black">Delete</font></A>
             </c:if>
               <a href="javascript:void(0)" onclick="addcommentjump(${list.id })">Reply</a>
            </TD>
            </TR>
             </c:if>
        </TABLE>
         </c:forEach>
        <BR/>
        <BR/>
        <c:choose>
        <c:when test="${empty user}">
        <div align="center">
           <font size=4>To write a comment, you need to
           <a href="/phoneaway/login"><font color="blue">sign in</font></a></font></br>
           </div>
        </c:when>
        <c:otherwise>
    <FORM  name="CommentForm" action="/phoneaway/community/add"  commandName="comment" method="POST"> 
            <DIV id="InputComment" style="text-align:center;">
                <TABLE cellSpacing="0" cellPadding="0" align="center" style="text-align:center;width:100%;margin-top:-50px;">
                    <TR>
                        <TD class= colSpan="3" style="font-size:20px;font-weight:700;height:30px;">Write a comment</TD>
                    </TR>
                    <TR>
                        <Td colSpan=3>
                            <DIV>   
                              <span><textarea style="WIDTH: 500px;" id="contents" name="content"  placeholder="content" rows="15" cols="90" tabIndex="2" ></textarea></span>
                            </DIV>
                          (the comment can't be larger than <FONT color="blue">250</FONT> words)
                        </Td>
                    </TR>
                </TABLE>
            </DIV>      
            <DIV style="MARGIN: 15px 0px; TEXT-ALIGN: center;width:100%;">
                <INPUT class="btn" tabIndex="3" style="padding:0; text-align:center; margin-right:50px;width:60px;height:30px;font-size:11px; color: #fff;background-color: #5cb85c;border-color: #4cae4c;" type="submit" value="SUBMIT"> 
                <INPUT class="btn" tabIndex="4" style="padding:0; text-align:center; width:60px;height:30px;font-size:11px; color: #fff;background-color: #5cb85c;border-color: #4cae4c;" type="reset"  value="CANCEL">
            </DIV>
        </FORM> 
        </c:otherwise>
        </c:choose>
    </DIV>
<footer>
        <div class="container" style="padding-top:20px;border-top:1px solid;">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Phone Away Website 2016</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="/phoneaway/policy">Privacy Policy</a>
                        </li>
                        <li><a href="/phoneaway/termuse">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
<script>
   function addcommentjump(refid) 
     {
      CommentForm['refId'].value=refid;
      location.hash="#InputComment"
     }
</script>
<!-- jQuery -->
 <script src="resources/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="resources/js/jqBootstrapValidation.js"></script>
<script src="resources/js/contact_me.js"></script>

<!-- Theme JavaScript -->
<script src="resources/js/agency.min.js"></script>


</body>
</html>