SET FOREIGN_KEY_CHECKS=0;
drop table if exists `User`;
drop table if exists `UserInfo`;
drop table if exists `Role`;
drop table if exists `Step`;
drop table if exists `OffTime`;
drop table if exists `AppUsage`;
SET FOREIGN_KEY_CHECKS=1;

CREATE TABLE `User` (
 	`id`  INT 	NOT NULL	AUTO_INCREMENT,
 	`email`     VARCHAR(255)    NOT NULL,
 	`username`	VARCHAR(30)		NOT NULL,
 	`create_date`   DATETIME    NOT NULL,
 	`password`      VARCHAR(255)   NOT NULL,

 	UNIQUE (`username`),
 	UNIQUE (`email`),
 	PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE `UserInfo` (
	`id`	INT		NOT NULL	AUTO_INCREMENT,
	`last_name`     VARCHAR(255),
	`first_name`    VARCHAR(255),
	`age`				INT,
	`gender`			VARCHAR(8),
	`country`        VARCHAR(255),
	`phone`         VARCHAR(25),
    `userid`		INT,

	PRIMARY KEY (`id`),
	FOREIGN KEY (`userid`) REFERENCES User(`id`)
) ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE `Role` (
	`id` INT 		NOT NULL	AUTO_INCREMENT,
	`rolename`		VARCHAR(50),
    `userid`		INT,

	PRIMARY KEY (`id`),
	FOREIGN KEY (`userid`) REFERENCES User(`id`)
) ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE `Step` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` INT  NOT NULL,
    `sdate` DATETIME NOT NULL,
    `steps` VARCHAR(200) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`)
        REFERENCES user (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE `OffTime` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` INT  NOT NULL,
    `odate` DATETIME NOT NULL,
    `olength` VARCHAR(200) NOT NULL,
    `done` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`)
        REFERENCES user (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE `AppUsage` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` INT  NOT NULL,
    `adate` DATETIME NOT NULL,
    `aname` VARCHAR(200) NOT NULL,
    `count` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`)
        REFERENCES user (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;
       