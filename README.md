# Introduction #

* Phone addiction: Mobile phone addiction is a dependence syndrome seen among certain mobile phone users. Some mobile phone users exhibit problematic behaviors related to substance use disorders. It can lead to increased time on mobile communication, adverse effects on relationships, and anxiety if separated from a mobile phone or sufficient signal.
* PhoneAway is a cure for phone addiction, with an app helping you stay away from your smartphone and focus on your work, and a web application letting you edit your profile and check your statistics.

### What is this application for? ###

* Function: The mobile app can record the time you leave your smartphone. You can set the time duration as you want. The timer will start when you click the bottom and stop if you open other application on your cell phone. The mobile app can also record the number of steps you walked by a pedometer. 
The web application let the user change their personal information, let them check their statistic data within a time period, and provide a ranking of their performance along with other users.
* Version: 1.0


### How do I get set up? ###

* Basic: Install the APK on your smartphone. Download the repository and run the code in Eclipse or STS.
* Configuration: Add server URL in baseActivity in mobile app.
* Built With:Spring MVC- The web framework used, Maven - Dependency Management
* Database configuration:


```
#!python

SET FOREIGN_KEY_CHECKS=0;
drop table if exists `User`;
drop table if exists `UserInfo`;
drop table if exists `Role`;
drop table if exists `Step`;
drop table if exists `OffTime`;
drop table if exists `AppUsage`;
SET FOREIGN_KEY_CHECKS=1;

CREATE TABLE `User` (
 	`id`  INT 	NOT NULL	AUTO_INCREMENT,
 	`email`     VARCHAR(255)    NOT NULL,
 	`username`	VARCHAR(30)		NOT NULL,
 	`create_date`   DATETIME    NOT NULL,
 	`password`      VARCHAR(255)   NOT NULL,

 	UNIQUE (`username`),
 	UNIQUE (`email`),
 	PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE `UserInfo` (
	`id`	INT		NOT NULL	AUTO_INCREMENT,
	`last_name`     VARCHAR(255),
	`first_name`    VARCHAR(255),
	`age`				INT,
	`gender`			VARCHAR(8),
	`country`        VARCHAR(255),
	`phone`         VARCHAR(25),
    `userid`		INT,

	PRIMARY KEY (`id`),
	FOREIGN KEY (`userid`) REFERENCES User(`id`)
) ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE `Role` (
	`id` INT 		NOT NULL	AUTO_INCREMENT,
	`rolename`		VARCHAR(50),
    `userid`		INT,

	PRIMARY KEY (`id`),
	FOREIGN KEY (`userid`) REFERENCES User(`id`)
) ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE `Step` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` INT  NOT NULL,
    `sdate` DATETIME NOT NULL,
    `steps` VARCHAR(200) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`)
        REFERENCES user (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE `OffTime` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` INT  NOT NULL,
    `odate` DATETIME NOT NULL,
    `olength` VARCHAR(200) NOT NULL,
    `done` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`)
        REFERENCES user (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE `AppUsage` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` INT  NOT NULL,
    `adate` DATETIME NOT NULL,
    `aname` VARCHAR(200) NOT NULL,
    `count` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`)
        REFERENCES user (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;
```


* Deployment instructions: the application should run on tomcat server.


### How do I use it? ###

* For user:
* Sign up: User must sign up and login to use the function in web application, user should sign up on website. Once user signup on website, users are able to use both website and Android mobile application after login. User will be navigate to homepage once they are successfully signup. The username and email is unique that means every user has a different username and different email User must input password twice to ensure they confirm the password. Sign up will create an user account with role of user and then create a default user profile with no information. The different error message will be displayed on screen after user click button signup. The error message will directly display on screen.
* Login: User must login after input the valid username and password. On website, the error message will be display at screen if user fail to login. Once user login, they are navigated to homepage and all available function display on screen. User name will be display on top right corner of the webpage. User could click their username, then visit user profile page and logout. Whether user navigate to different pages, the username will always display at the top right corner to allow user visit profile and logout.
* Logout: After user login, username has been display at top right corner of the webpage. After click the username, users are able to logout at any webpages.
* Profile: After user login, users are able to click their username and find their profile information. The profile information contain the account information and user personal information. Users are able to update their account information such as change password, change email. The username cannot be changed to keep user be unique and avoid the abuse to change username. User also need to type password twice to ensure the password has been confirmed before modified. For user personal information, users must input valid first name, valid last name, valid age, select gender, valid country, valid phone number. User could update the personal information they want to, and they could keep blank if there are some information they don't want to updated in profile. All input personal information has been validated by system to ensure all input information is valid. The success message will display at screen and error message will shown as pop-up alert window. The user personal information will saved in individual table to ensure the security of privacy. The personal information cannot be visit by anyone except user themselves.
* Dashboard: Users could use search bar to search the off-phone time by date, the most used application and the application used history. All search result will be displayed on webpage and shown as bar chart and pie chart. And they are able to view the off-phone time and app usage as pie chart and bar chart by date, and the username of complete most goals.
* Leaderboard(no need login): Any visitors of our website is able to user this function. Visitors are able to view the rank of top users who have higher off-phone time on today. And view the rank of top users who have walked the most steps on today.
* Statistics: User are able to view the statistic of steps walked by day, week, and month, and all of the statistics are shown as list.
* Mobile set off-phone time goal/offtime recorder: Any registered users could set off-time goal in setting page. The period ranges from 0 to 120 mins.Any registered users can use this function by clicking start bottom after logging in the mobile application.The user can set any time duration as they like. Once the users open other application the timer will stop counting time, our app will send a notification which indicates that the time duration which users off their mobile phone and send the time duration to our backend database.
* Mobile record step: When the users carry their mobile phones with them, the app will automatically record the count of steps by the step sensors.
* Mobile view statistic: Users could check the off-phone time and steps by date, along with the most used application and the application used history. All result will be displayed on mobile application and shown as bar chart and pie chart in different interface. And they are able to view the step counts, distance and time duration in step page.

* For administrator/Backend: TThis system provide backend user account management. The administrator is able to login with url: phoneaway/backend. The default administrator account username is admin, password is admin. Once administrator login the backend, all user account information will be shown as list. Administrator is able to search specific user by username or add a new user account. Administrator is able to delete any of the users and modify the account information includes username, password, and email address.

### Go through ###
* Website: Website is used to display related user information such as leaderboard, statistic and dashboard. User need to click login/signup button, and then they could login (if they are not signup, they could click signup here to create a new account). After that, users are able to click they username to update user profile include user account information and user personal information. They are able to view dashboard and statistic after click related button. Leaderboard display the ranking of all users by phone-off time. Dashboard contains the information of all app's usage on mobile phone that effect users, these will help user understand which application effect them a lot. Statistic will display the information of their steps based on Google map. These information will encourage user go outside and take walk rather than using phone. And users are able to logout at any webpages on website. 
* Mobile: Mobil App is used to collect data regarding no-phone time, app usage, and steps, map. The users could also set off-time goals in setting interface, view statistic information. The users can only use the function mentioned above after registration and logging in the app.

###API Description ###
* 1.http://10.19.97.186:8080/phoneaway/login/getUser 
*   	 	-login       
* 2.http://10.19.97.186:8080/phoneaway/offtime/add   
*   	 	-add offtime data     
* 3.http://10.19.97.186:8080/phoneaway/offtime/getTime     
*  		-get all offtime data
* 4.http://10.19.97.186:8080/phoneaway/offtime/queryTimeList  
*  		-get offtime data between start_date and end_date
* 5.http://10.19.97.186:8080/phoneaway/app/add
*  	　　    -add app usage data
* 6.http://10.19.97.186:8080/phoneaway/app/getApp
*  		-get all app usage data
* 7.http://10.19.97.186:8080/phoneaway/app/queryAppList
*  		-get app data between start_date and end_date
* 8.http://10.19.97.186:8080/phoneaway/step/add
*  		-add steps data